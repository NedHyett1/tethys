﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;

using Tethys.Utility;
using Tethys.Utility.Streams;

namespace Tethys.Transport
{
    /// <summary>
    /// This class is me basically talking out of my arse about how to create a platform agnostic class encoder.
    /// It probably sucks HUGE balls in terms of efficiency but it should allow .NET to serialise objects in a
    /// way that can (should) easily be implemented in other languages without relying on the structure of .NET
    /// too heavily.
    ///
    /// Notes:
    /// - The encoder does not differentiate between props and fields in the encoded data except where they are placed in the encode. TODO: merge these
    /// - The encoder requires that all models have a blank constructor
    /// - The encoder requires a well defined enumerable to be able to re-decode the enumerable back into the proper types
    ///
    /// TODO: finish this
    /// </summary>
    /// <typeparam name="TModel"></typeparam>
    public class ModelEncoder<TModel>
    {
        private static readonly Dictionary<Type, string> CommonTypeIds = new Dictionary<Type, string>
        {
            {typeof(bool), "BOOL" },
            {typeof(char), "CHAR" },
            {typeof(byte), "BYTE" },
            {typeof(string), "STRING" },
            {typeof(short), "INT16" },
            {typeof(ushort), "UINT16" },
            {typeof(int), "INT32" },
            {typeof(uint), "UINT32" },
            {typeof(long), "INT64" },
            {typeof(ulong), "UINT64" },
            {typeof(float), "FLOAT32" },
            {typeof(double), "FLOAT64" },
            {typeof(decimal), "FLOAT128" },
            {typeof(Array), "ARRAY" },
            {typeof(IEnumerable<>), "LIST" },
            {typeof(IEnumerable<>).MakeGenericType(typeof(KeyValuePair<,>)), "DICTIONARY" }
        };

        private static Dictionary<string, Type> CommonTypesFromIds => CommonTypeIds.ToList().ToDictionary(kvp => kvp.Value, kvp => kvp.Key);

        private static readonly Dictionary<Type, string> CustomTypes = new Dictionary<Type, string>();

        public static void RegisterType(Type t, string id)
        {
            CustomTypes.Add(t, id);
        }

        public static string GetTypeId(Type tin, string rval = "CLASS", bool inclEnum = true)
        {
            if (CommonTypeIds.Any(t => t.Key == tin || t.Key.IsAssignableFrom(tin)))
            {
                var common = CommonTypeIds.FirstOrDefault(t => t.Key == tin || t.Key.IsAssignableFrom(tin)).Value;

                if (!inclEnum)
                {
                    if (common == "ARRAY" || common == "LIST" || common == "DICTIONARY")
                        return rval;
                }

                return common;
            }

            return rval;
        }

        /**
         * Encoding ->
         *
         * - If class, check if class is registered. If not:
         *   - Check if serialisable
         *   - Check for serialisable name attr
         *   - Register using serialisable name attr
         *
         * - If top level encode, write the type ID.
         *   - If class, write custom class ID
         *
         * - If not a class (rather a default type):
         *   - Encode the type and end
         *
         * - If is a class:
         *   - Enumerate non static fields and properties.
         *   - Write number of fields and write each field
         *   - Write number of properties and write each property
         *
         * DURING EACH WRITE THE ALGORITHM CAN RECURSE INTO ITSELF BY
         * CREATING A NEW MODEL ENCODER FOR THE TYPE.
         */

        public void Encode(TModel model, SimpleStream stream, bool topLevel = true)
        {
            if (topLevel)
            {
                stream.Write(GetTypeId(model.GetType()));
                if (CustomTypes.ContainsKey(model.GetType()))
                {
                    stream.Write(CustomTypes[model.GetType()]);
                }
            }

            if (GetTypeId(model.GetType()) != "CLASS")
            {
                EncodeValue(model.GetType(), model, stream);
                return;
            }

            var fields = model.GetType().GetFields().Where(f => !f.IsNotSerialized && !f.IsStatic);
            var props = model.GetType().GetProperties();

            stream.Write(fields.Count() + props.Length); //Lump them together because other languages may not see the difference

            foreach (var fieldInfo in fields)
            {
                EncodeField(model, fieldInfo, stream);
            }

            foreach (var propertyInfo in props)
            {
                EncodeProperty(model, propertyInfo, stream);
            }
        }

        /// <summary>
        /// Wrap the field into an abstraction layer so it doesn't care about how to read/write the
        /// value.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="field"></param>
        /// <param name="stream"></param>
        private void EncodeField(TModel model, FieldInfo field, SimpleStream stream)
        {
            EncodeMember(model, new FieldWrapper(field), stream);
        }

        /// <summary>
        /// Wrap the property into an abstraction layer so it doesn't care about how to read/write the
        /// value.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="prop"></param>
        /// <param name="stream"></param>
        private void EncodeProperty(TModel model, PropertyInfo prop, SimpleStream stream)
        {
            EncodeMember(model, new PropWrapper(prop), stream);
        }

        /// <summary>
        /// Convergence point from the abstraction wrappers.
        /// </summary>
        /// <param name="model"></param>
        /// <param name="member"></param>
        /// <param name="stream"></param>
        private void EncodeMember(TModel model, IMemberWrapper member, SimpleStream stream)
        {
            var mType = member.GetMemberType();
            var mVal = member.Get(model);
            stream.Write(member.GetMemberName());
            EncodeValue(mType, mVal, stream);
        }

        /// <summary>
        /// Perform raw encode of a value
        /// </summary>
        /// <param name="mType">the type of thing we are encoding</param>
        /// <param name="mVal">the thing to encode</param>
        /// <param name="stream">the stream to encode to</param>
        private void EncodeValue(Type mType, object mVal, SimpleStream stream)
        {
            var typeIdN = GetTypeId(mType, null);
            if (mVal is Array arr) //get arrays first
            {
                stream.Write("ARRAY");
                var arrayType = mType.GetElementType();
                var arrayTypeStr = GetTypeId(arrayType);
                stream.Write(arrayTypeStr);
                stream.Write(arr.Length);
                foreach (var element in arr)
                {
                    SubModelEncode(element, stream);
                }
            }
            else if (mType.IsGenericType && typeof(IDictionary<,>).IsAssignableFrom(mType.GetGenericTypeDefinition())) //then dictionaries
            {
                stream.Write("DICTIONARY");
                //fetch count method
                //fetch indexer
                //write kvp count
                //write keytype
                //write valtype
                //for each kvp:
                //  get kvp from indexer
                //  write key as keytype
                //  write value as valtype
            }
            else if (mType.IsGenericType && typeof(IEnumerable<>).IsAssignableFrom(mType.GetGenericTypeDefinition())) //then every other enumerable
            {
                stream.Write("LIST");
                //fetch count method
                //fetch indexer
                //write count
                //write member type
                //for each member:
                //  get member from indexer
                //  write member as member type
            }
            else if (typeIdN != null)
            {
                stream.Write(typeIdN);
                switch (typeIdN)
                {
                    case "BOOL":
                        {
                            stream.Write((bool)mVal);
                        }
                        break;

                    case "CHAR":
                        {
                            stream.Write((char)mVal);
                        }
                        break;

                    case "BYTE":
                        {
                            stream.Write(new[] { (byte)mVal });
                        }
                        break;

                    case "STRING":
                        {
                            stream.Write((string)mVal);
                        }
                        break;

                    case "INT16":
                        {
                            stream.Write((short)mVal);
                        }
                        break;

                    case "UINT16":
                        {
                            stream.Write((ushort)mVal);
                        }
                        break;

                    case "INT32":
                        {
                            stream.Write((int)mVal);
                        }
                        break;

                    case "UINT32":
                        {
                            stream.Write((uint)mVal);
                        }
                        break;

                    case "INT64":
                        {
                            stream.Write((long)mVal);
                        }
                        break;

                    case "UINT64":
                        {
                            stream.Write((ulong)mVal);
                        }
                        break;

                    case "FLOAT32":
                        {
                            stream.Write((float)mVal);
                        }
                        break;

                    case "FLOAT64":
                        {
                            stream.Write((double)mVal);
                        }
                        break;

                    case "FLOAT128":
                        {
                            stream.Write((decimal)mVal);
                        }
                        break;

                    default:
                        throw new Exception($"Invalid type '{GetTypeId(mType)}' during encode.");
                }
            }
            else if (mType.IsSerializable) //Else just perform a serialisable encode
            {
                SubModelEncode(mVal, stream);
            }
        }

        /// <summary>
        /// Allows the algorithm to recurse until the entire model is degenerated into primitives.
        /// </summary>
        /// <param name="sub"></param>
        /// <param name="stream"></param>
        private static void SubModelEncode(object sub, SimpleStream stream)
        {
            var genericEncoderType = typeof(ModelEncoder<>).MakeGenericType(sub.GetType());
            var genericEncoder = Activator.CreateInstance(genericEncoderType);
            genericEncoderType.GetMethod("Encode").Invoke(genericEncoder, new[] { sub, stream, false });
        }

        /**
         * Decoding ->
         * - If top level decode:
         *   - Read the type ID
         *   - If class, read the custom class ID
         *      - Check if read class type matches generic model type (doesn't matter in sub-decodes because the encoder would write the EXACT class type)
         *      - If matches, proceed as normal
         *      - If doesn't match, check if generic model type has been extended to read type
         *      - If does extend, rewrite the model created by the activator to use the extended type but
         *        pretend that it is still the lower type (reflection should pull the members from the higher type)
         *
         * - If not a class (rather a default type):
         *   - Decode the type and end
         *
         * - If is a class:
         *   - Enumerate non static fields and properties and convert them to member abstractions
         *   - Read number of fields (validate against enumerated model fields) and read all fields into an array
         *   - Match the read fields with the abstractions and write the read data into the model through the abstraction
         *   - Read number of properties (validate against enumerated model properties) and read all properties into an array
         *   - Match the read properties with the abstractions and write the read data into the model through the abstraction
         *
         *
         * DURING EACH READ THE ALGORITHM CAN RECURSE INTO ITSELF BY
         * CREATING A NEW MODEL ENCODER FOR THE TYPE.
         */

        public TModel Decode(SimpleStream stream, bool topLevel = true)
        {
            TModel theModel;
            if (topLevel)
            {
                var typeId = stream.ReadString();
                if (typeId == "CLASS")
                {
                    var classId = stream.ReadString(); //The read class ID
                    var operatingClassId = ""; //The generic class ID
                    if (CustomTypes.ContainsKey(typeof(TModel)))
                    {
                        operatingClassId = CustomTypes[typeof(TModel)];
                    }
                    else
                    {
                        throw new SerializationException($"Failed to get typeid for model {typeof(TModel)}");
                    }

                    //Are we wrong?
                    if (operatingClassId != classId)
                    {
                        //If wrong, get the type for the read ID, check for inheritance, if it all checks out, create a model from
                        //the readID type and assign to 'theModel', else we throw
                        var flipped = CustomTypes.FlipKeyValuePairs();
                        if (!flipped.ContainsKey(classId))
                            throw new SerializationException($"Failed to get model type for typeid {classId}");
                        var readType = flipped[classId];
                        if (!typeof(TModel).IsAssignableFrom(readType))
                            throw new SerializationException($"Model type {readType.FullName} is not a child of {typeof(TModel).FullName}");
                        theModel = (TModel)Activator.CreateInstance(readType);
                    }
                    else
                    {
                        //We are right, so just create an instance of the generic
                        theModel = Activator.CreateInstance<TModel>();
                    }
                }
                else
                {
                    //If we aren't decoding a class then just believe the generic because we won't know better
                    theModel = Activator.CreateInstance<TModel>();
                }
            }
            else
            {
                //We can assume that TModel is right because we are operating on information provided by the encoder
                //instead of the end user (who might be requesting a superclass)
                theModel = Activator.CreateInstance<TModel>();
            }

            if (GetTypeId(theModel.GetType()) != "CLASS")
            {
                //Explicit cast should work because we explicitly asked it to decode
                //the model type we are casting to
                theModel = (TModel)DecodeValue(theModel.GetType(), stream);
                return theModel;
            }

            //Just lumping them together should work.
            var members = new List<IMemberWrapper>();
            members.AddRange(theModel.GetType().GetFields().Where(f => !f.IsNotSerialized && !f.IsStatic).Select(f => new FieldWrapper(f)));
            members.AddRange(theModel.GetType().GetProperties().Select(p => new PropWrapper(p)));

            for (var i = 0; i < members.Count; i++)
            {
                //Pass members by ref to avoid memory thrashing (I think that's how it works...)
                DecodeMember(ref theModel, ref members, stream);
            }

            return theModel;
        }

        private void DecodeMember(ref TModel model, ref List<IMemberWrapper> members, SimpleStream stream)
        {
            var memberName = stream.ReadString();
            var memberType = stream.ReadString();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="mType">the type of thing we are decoding</param>
        /// <param name="stream">the stream to decode from</param>
        /// <returns>the decoded thing</returns>
        private object DecodeValue(Type mType, SimpleStream stream)
        {
            return null;
        }

        /// <summary>
        /// Allows the algorithm to recurse until the entire model is tree has been explored
        /// </summary>
        /// <param name="sub"></param>
        /// <param name="stream"></param>
        private static void SubModelDecode(object sub, SimpleStream stream)
        {
            var genericEncoderType = typeof(ModelEncoder<>).MakeGenericType(sub.GetType());
            var genericEncoder = Activator.CreateInstance(genericEncoderType);
            genericEncoderType.GetMethod("Decode").Invoke(genericEncoder, new object[] { stream, false });
        }
    }

    internal interface IMemberWrapper
    {
        object Get(object inst);

        void Set(object inst, object val);

        Type GetMemberType();

        string GetMemberName();
    }

    internal class FieldWrapper : IMemberWrapper
    {
        private FieldInfo fi;

        public FieldWrapper(FieldInfo fi)
        {
            this.fi = fi;
        }

        public object Get(object inst)
        {
            return fi.GetValue(inst);
        }

        public void Set(object inst, object val)
        {
            fi.SetValue(inst, val);
        }

        public Type GetMemberType()
        {
            return fi.FieldType;
        }

        public string GetMemberName()
        {
            return fi.Name;
        }
    }

    internal class PropWrapper : IMemberWrapper
    {
        private PropertyInfo pi;

        public PropWrapper(PropertyInfo pi)
        {
            this.pi = pi;
        }

        public object Get(object inst)
        {
            return pi.GetGetMethod().Invoke(inst, new object[0]);
        }

        public void Set(object inst, object val)
        {
            pi.GetSetMethod().Invoke(inst, new[] { val });
        }

        public Type GetMemberType()
        {
            return pi.PropertyType;
        }

        public string GetMemberName()
        {
            return pi.Name;
        }
    }
}