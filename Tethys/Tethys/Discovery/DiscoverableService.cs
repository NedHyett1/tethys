﻿using System;
using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Newtonsoft.Json;

namespace Tethys.Discovery
{
    /// <summary>
    /// Publish information about a service that is being advertised by this host.
    /// </summary>
    public class DiscoverableService
    {
        private readonly UdpClient _socket;

        private bool _die = false;
        private Thread _thread;

        private readonly ServiceConfiguration _serviceConfiguration;

        public DiscoverableService(int port, ServiceConfiguration serviceConfiguration)
        {
            _serviceConfiguration = serviceConfiguration ?? throw new ArgumentNullException(nameof(serviceConfiguration));

            var ipep = new IPEndPoint(IPAddress.Any, port);
            _socket = new UdpClient(ipep);
        }

        /// <summary>
        /// Listens for requests for service discovery and responds if they match the service configuration.
        /// </summary>
        private void ListenLoop()
        {
            var sender = new IPEndPoint(IPAddress.Any, 0);

            while (!_die)
            {
                var data = _socket.Receive(ref sender);
                try
                {
                    var message = JsonConvert.DeserializeObject<DiscoveryRequest>(Encoding.ASCII.GetString(data));
                    if (message.Magic != TethysSettings.ServiceDiscoveryId)
                        continue;

                    if (!_serviceConfiguration.FilterDiscoveryRequest(message))
                        continue;

                    var resp = new DiscoveryResponse
                    {
                        Magic = TethysSettings.ServiceDiscoveryId,
                        Hostname = ((IPEndPoint)_socket.Client.LocalEndPoint).Address
                    };

                    _serviceConfiguration?.ConfigureServiceResponse(ref resp);

                    var outBytes = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(resp));

                    _socket.Send(outBytes, outBytes.Length, sender);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        public void Start()
        {
            if (_thread != null)
                throw new Exception("Cannot start already running service");

            _die = false;
            _thread = new Thread(ListenLoop) { IsBackground = true };
            _thread.Start();
        }

        public void Stop()
        {
            _die = true;
            _thread = null;
        }
    }
}