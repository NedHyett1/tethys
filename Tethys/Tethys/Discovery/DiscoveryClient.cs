﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Newtonsoft.Json;

namespace Tethys.Discovery
{
    public class DiscoveryClient
    {
        public Guid ServiceId { get; }

        public int Port { get; }

        private readonly UdpClient _socket;

        private bool _die;

        private readonly int? _queryInterval;

        private readonly ServiceConfiguration _serviceConfiguration;

        public event EventHandler<DiscoveryResponse> ServiceDiscovered;

        public DiscoveryClient(int port, Guid serviceId, ServiceConfiguration svConfig, int? queryInterval = null)
        {
            Port = port;
            var ipep = new IPEndPoint(IPAddress.Any, port);
            _socket = new UdpClient(ipep)
            {
                EnableBroadcast = true
            };
            ServiceId = serviceId;
            _queryInterval = queryInterval;
            _serviceConfiguration = svConfig;
        }

        private static bool IsLocalIpAddress(string host)
        {
            try
            {
                var hostIps = Dns.GetHostAddresses(host);
                var localIps = Dns.GetHostAddresses(Dns.GetHostName());
                foreach (var hostIp in hostIps)
                {
                    if (IPAddress.IsLoopback(hostIp))
                        return true;
                    if (localIps.Contains(hostIp))
                        return true;
                }
            }
            catch
            {
                // ignored
            }
            return false;
        }

        private void ListenLoop()
        {
            var sender = new IPEndPoint(IPAddress.Any, 0);

            while (!_die)
            {
                var data = _socket.Receive(ref sender);
                if (IsLocalIpAddress(sender.Address.ToString()))
                    continue;
                try
                {
                    var message = JsonConvert.DeserializeObject<DiscoveryResponse>(Encoding.ASCII.GetString(data));
                    if (message.Magic != TethysSettings.ServiceDiscoveryId)
                        continue;

                    ServiceDiscovered?.Invoke(this, message);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        private void QueryLoop()
        {
            while (!_die)
            {
                try
                {
                    Discover();
                    // ReSharper disable once PossibleInvalidOperationException
                    Thread.Sleep(TimeSpan.FromSeconds(_queryInterval.Value));
                }
                catch (Exception ex)
                {
                    Debug.WriteLine(ex);
                }
            }
        }

        public void Start()
        {
            _die = false;
            var thread = new Thread(ListenLoop) { IsBackground = true };
            thread.Start();
            if (_queryInterval != null)
            {
                var qThread = new Thread(QueryLoop) { IsBackground = true };
                qThread.Start();
            }
        }

        public void Stop()
        {
            _die = true;
        }

        public void Discover()
        {
            var req = _serviceConfiguration.GenerateRequest();

            req.Magic = TethysSettings.ServiceDiscoveryId;
            req.ServiceId = ServiceId;

            var reqBytes = Encoding.ASCII.GetBytes(JsonConvert.SerializeObject(req));
            _socket.Send(reqBytes, reqBytes.Length, new IPEndPoint(IPAddress.Broadcast, Port));
        }
    }
}