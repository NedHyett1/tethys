﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Tethys.Discovery
{
    public class DiscoveryResponse
    {
        public Guid Magic { get; set; }

        public Guid ServiceId { get; set; }

        public IPAddress Hostname { get; set; }

        public int Port { get; set; }

        public Dictionary<string, object> ResponseMetadata { get; set; } = new Dictionary<string, object>();
    }
}