﻿using System;

namespace Tethys.Discovery
{
    public abstract class ServiceConfiguration
    {
        public abstract DiscoveryRequest GenerateRequest();

        public abstract bool FilterDiscoveryRequest(DiscoveryRequest request);

        public abstract void ConfigureServiceResponse(ref DiscoveryResponse response);

        public abstract Guid GetServiceId(DiscoveryResponse response);
    }
}