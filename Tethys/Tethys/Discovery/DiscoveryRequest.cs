﻿using System;
using System.Collections.Generic;

namespace Tethys.Discovery
{
    public class DiscoveryRequest
    {
        public Guid Magic { get; set; }

        public Guid ServiceId { get; set; }

        public Dictionary<string, object> RequestMetadata { get; set; } = new Dictionary<string, object>();
    }
}