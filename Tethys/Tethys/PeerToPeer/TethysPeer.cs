﻿using Tethys.Server;
using Tethys.Sockets;

namespace Tethys.PeerToPeer
{
    public class TethysPeer : TethysConnectedClient
    {
        public TethysPeer(ITethysServer server, ITethysSocketAdapter socket) : base(server, socket)
        {
        }
    }
}