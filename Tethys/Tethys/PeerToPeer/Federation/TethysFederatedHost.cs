﻿using System;

using Tethys.Server;

namespace Tethys.PeerToPeer.Federation
{
    public class TethysFederatedHost : IDisposable
    {
        public TethysPeerToPeerHost P2PHost { get; }
        public TethysServer Gateway { get; }

        public TethysFederatedHost(int p2pPort, int gatewayPort, Guid? connectionId = null)
        {
            P2PHost = new TethysPeerToPeerHost(p2pPort, connectionId);
            Gateway = new TethysServer(gatewayPort, P2PHost.PeerId);
        }

        public void ConnectToPeer(string address)
        {
            P2PHost.ConnectTo(address);
        }

        public void Dispose()
        {
            P2PHost.Dispose();
            Gateway.Dispose();
        }
    }
}