﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

using Tethys.Packet;
using Tethys.PacketEncoding;
using Tethys.Security;
using Tethys.Server;
using Tethys.Sockets;
using Tethys.Sockets.SocketAdapters;

namespace Tethys.PeerToPeer
{
    /// <summary>
    /// A modified TethysServer that acts as both a client and a server. Intended for federated systems.
    /// </summary>
    public class TethysPeerToPeerHost : ITethysServer, IDisposable
    {
        public Guid PeerId { get; }
        public int Port { get; }
        public IPacketEncoder PacketEncoder { get; private set; }

        private readonly TethysInboundConnectionListener _inboundConnectionListener;

        private readonly ConcurrentDictionary<Guid, TethysPeer> _peers = new ConcurrentDictionary<Guid, TethysPeer>();

        /// <inheritdoc />
        /// <summary>
        /// Create a new TethysP2PHost.
        /// </summary>
        /// <param name="port">The port that the P2P Host operates on.</param>
        /// <param name="peerId"></param>
        public TethysPeerToPeerHost(int port, Guid? peerId = null, IPacketEncoder packetEncoder = null)
        {
            Port = port;
            PeerId = peerId.GetValueOrDefault(Guid.NewGuid());

            PacketEncoder = packetEncoder ?? new DotNetBinaryPacketEncoder();

            _inboundConnectionListener = new TethysInboundConnectionListener(port);
            _inboundConnectionListener.ClientConnected += OnClientConnected;
        }

        private void OnClientConnected(object sender, ITethysSocketAdapter e)
        {
            var tClient = new TethysPeer(this, e);
            //Ask the client who they are
            var iam = tClient.WriteAndAwaitResponseAsync<TethysIdentifyPacket>(new TethysIdentifyPacket()).GetAwaiter().GetResult();
            tClient.ConnectionId = iam.Id;

            if (_peers.ContainsKey(tClient.ConnectionId))
            {
                tClient.Disconnect("Already connected");
                tClient.Dispose();
                return;
            }

            var ipAddresses = _peers.Values.ToList().ConvertAll(cl => cl.IpAddress.ToString()).ToArray();
            tClient.WritePacket(new TethysPeerToPeerSharePeersPacket
            {
                PeerAddresses = ipAddresses
            });

            _peers.TryAdd(tClient.ConnectionId, tClient);
        }

        public void ConnectTo(string address)
        {
            var client = new TcpSocketAdapter(address, Port);
            var peer = new TethysPeer(this, client);

            //Ask the client who they are
            var iam = peer.WriteAndAwaitResponseAsync<TethysIdentifyPacket>(new TethysIdentifyPacket()).GetAwaiter().GetResult();
            peer.ConnectionId = iam.Id;

            if (_peers.ContainsKey(peer.ConnectionId))
            {
                peer.Disconnect("Already connected");
                peer.Dispose();
                return;
            }

            var ipAddresses = _peers.Values.ToList().ConvertAll(cl => cl.IpAddress.ToString()).ToArray();
            peer.WritePacket(new TethysPeerToPeerSharePeersPacket
            {
                PeerAddresses = ipAddresses
            });

            _peers.TryAdd(peer.ConnectionId, peer);
        }

        public void Dispose()
        {
            _inboundConnectionListener.Dispose();
        }

        public Guid GetConnectionId()
        {
            return PeerId;
        }

        public void Broadcast(TethysPacket packet)
        {
            var tasks = new List<Task>();
            foreach (var client in _peers)
            {
                tasks.Add(client.Value.WritePacketAsync(packet));
            }
            Task.WhenAll(tasks).GetAwaiter().GetResult();
        }

        public Task BroadcastAsync(TethysPacket packet)
        {
            return Task.CompletedTask.ContinueWith(t => { Broadcast(packet); });
        }

        public Task<(Guid, TPacket)[]> BroadcastAndAwaitResponseAsync<TPacket>(TethysPacket packet) where TPacket : TethysPacket
        {
            var tasks = new List<Task<(Guid, TPacket)>>();
            foreach (var client in _peers)
            {
                tasks.Add(Task.CompletedTask.ContinueWith(t =>
                {
                    var clientId = client.Value.ConnectionId;
                    var response = client.Value.WriteAndAwaitResponseAsync<TPacket>(packet).GetAwaiter().GetResult();
                    return (clientId, response);
                }));
            }

            return Task.WhenAll(tasks);
        }

        public TethysConnectedClient GetClient(Guid clientId)
        {
            return _peers[clientId];
        }

        public void DisconnectClient(Guid clientId, string reason = null)
        {
            _peers.TryRemove(clientId, out var ret);
            ret.Dispose();
        }

        public IPacketEncoder GetPacketEncoder()
        {
            return PacketEncoder;
        }

        public void SetTransportService(IPacketEncoder service)
        {
            PacketEncoder = service;
        }

        public Guid PrepareForBinaryAsideStream(string protocol, bool side)
        {
            throw new NotImplementedException();
        }

        public SSLContainer GetSslConnectionProperties()
        {
            throw new NotImplementedException();
        }

        public void Start()
        {
            throw new NotImplementedException();
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public Guid PrepareForBinaryStream(string protocol)
        {
            throw new NotImplementedException();
        }

        public Task<Stream> EstablishBinaryStream(string protocol)
        {
            throw new NotImplementedException();
        }
    }
}