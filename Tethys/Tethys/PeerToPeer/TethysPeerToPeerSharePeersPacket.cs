﻿using System;

using Tethys.Client;
using Tethys.Packet;
using Tethys.Server;

namespace Tethys.PeerToPeer
{
    public class TethysPeerToPeerSharePeersPacket : TethysPacket
    {
        public string[] PeerAddresses { get; set; }

        public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
        {
            var host = (TethysPeerToPeerHost)server;
            foreach (var peerAddress in PeerAddresses)
            {
                host.ConnectTo(peerAddress);
            }
        }

        public override void ProcessPacketClient(TethysClient client)
        {
            throw new NotImplementedException(); //p2p has no clients
        }
    }
}