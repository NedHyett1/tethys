﻿namespace Tethys
{
    public enum TethysSide
    {
        Both,
        Client,
        Server,
        Unknown
    }
}