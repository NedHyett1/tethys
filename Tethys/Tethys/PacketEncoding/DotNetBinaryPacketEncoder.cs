﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Tethys.PacketEncoding
{
    public class DotNetBinaryPacketEncoder : IPacketEncoder
    {
        public byte[] SerialiseObject(object input)
        {
            var bf = new BinaryFormatter();
            using (var ms = new MemoryStream())
            {
                bf.Serialize(ms, input);
                return ms.ToArray().Compress();
            }
        }

        public T DeserialiseObject<T>(byte[] input) where T : class
        {
            using (var ms = new MemoryStream())
            {
                var bf = new BinaryFormatter();
                var decompInput = input.Decompress();
                ms.Write(decompInput, 0, decompInput.Length);
                ms.Seek(0, SeekOrigin.Begin);
                var objOut = bf.Deserialize(ms);
                return objOut as T;
            }
        }
    }
}