﻿using Newtonsoft.Json;

namespace Tethys.PacketEncoding
{
    public class JsonPacketEncoder : IPacketEncoder
    {
        public byte[] SerialiseObject(object input)
        {
            var json = JsonConvert.SerializeObject(input, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            });
            var jsonBytes = System.Text.Encoding.UTF8.GetBytes(json);
            return jsonBytes;
        }

        public T DeserialiseObject<T>(byte[] input) where T : class
        {
            var json = System.Text.Encoding.UTF8.GetString(input);
            return JsonConvert.DeserializeObject(json, new JsonSerializerSettings
            {
                TypeNameHandling = TypeNameHandling.All
            }) as T;
        }
    }
}