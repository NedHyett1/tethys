﻿namespace Tethys.PacketEncoding
{
    public interface IPacketEncoder
    {
        byte[] SerialiseObject(object input);

        T DeserialiseObject<T>(byte[] input) where T : class;
    }
}