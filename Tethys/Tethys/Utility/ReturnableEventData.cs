﻿namespace Tethys.Utility
{
    public class ReturnableEventData<TType, TReturnType>
    {
        public TType Data { get; set; }
        public TReturnType Result { get; set; }
    }
}