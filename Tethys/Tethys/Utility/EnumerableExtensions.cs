﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tethys.Utility
{
    public static class EnumerableExtensions
    {
        public static IDictionary<TVal, TKey> FlipKeyValuePairs<TVal, TKey>(this IEnumerable<KeyValuePair<TKey, TVal>> enumerable)
        {
            return enumerable.ToDictionary(e => e.Value, e => e.Key);
        }
    }
}