﻿using System;
using System.Threading;

namespace Tethys.Utility
{
    /// <summary>
    /// Await the abstract completion of any task that gives data to this lock.
    ///
    /// T1 -> Awaits on <see cref="Get"/>
    /// T2 -> Finishes work and calls <see cref="Give"/>
    /// T2 -> Awaits on <see cref="AwaitReset"/>
    /// T1 -> Takes the variable from T2, resets the lock and fires the reset command, then processes the value
    /// T2 -> Runs past <see cref="AwaitReset"/>, generates a new data and goes back to step one.
    /// </summary>
    /// <typeparam name="TDataType"></typeparam>
    public class SynchronisedExchangeLock<TDataType>
    {
        /// <summary>
        /// Triggered when thing is given
        /// </summary>
        private readonly CountdownEvent _cde;

        /// <summary>
        /// Triggered when the taker has reset the given latch
        /// </summary>
        private readonly CountdownEvent _cdeR;

        private TDataType _data;

        public SynchronisedExchangeLock()
        {
            this._cde = new CountdownEvent(1);
            this._cdeR = new CountdownEvent(1);
            this._cdeR.Signal(); //Unlock at start
        }

        public void Give(TDataType data)
        {
            //Give the data to the lock
            _data = data;
            //Signal the all clear
            _cde.Signal();
        }

        public TDataType Get(TimeSpan? timeout = null)
        {
            //Await the data
            if (timeout.HasValue) _cde.Wait(timeout.Value); else _cde.Wait();
            //Cache the data
            var dataCache = _data;
            //Reset the data to default
            _data = default;
            //Reset the main awaiter
            _cde.Reset();
            //Signal that it is all clear for T2 to begin
            _cdeR.Signal();
            //Give the data back
            return dataCache;
        }

        public void AwaitReset()
        {
            //Await the all clear from T1
            _cdeR.Wait();
            //Reset the all clear signal
            _cdeR.Reset();
        }
    }
}