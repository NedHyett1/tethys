﻿using System.Text;

using Newtonsoft.Json;

namespace Tethys.Utility
{
    public static class StandardExtensions
    {
        public static byte[] ToJsonBytes(this object o)
        {
            var json = JsonConvert.SerializeObject(o);
            var jsonBytes = Encoding.UTF8.GetBytes(json);
            return jsonBytes;
        }
    }
}