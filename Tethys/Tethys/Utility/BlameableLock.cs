﻿using System;
using System.Diagnostics;
using System.Threading;

namespace Tethys.Utility
{
    /// <summary>
    /// A lock that blames the thread that locked it, allowing the lock to interrupt a blocking call inside the action.
    /// </summary>
    public class BlameableLock
    {
        private readonly object _lock = new object();
        private Thread _lockingThread;
        private bool _nonBlocking = true; //true if this is a safe operation and shouldn't be breakable

        public void Lock(Action action, bool nonBlocking = true)
        {
            try
            {
                Enter();
                _nonBlocking = nonBlocking;
                _lockingThread = Thread.CurrentThread;
                action.Invoke();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in BlameableLock:");
                Debug.WriteLine(ex);
                _lockingThread = null;
            }
            finally
            {
                if (Monitor.IsEntered(_lock))
                {
                    _lockingThread = null;
                    Monitor.Exit(_lock);
                }
            }
        }

        public TOut Lock<TOut>(Func<TOut> action, bool nonBlocking = true)
        {
            try
            {
                Enter();
                _nonBlocking = nonBlocking;
                _lockingThread = Thread.CurrentThread;
                return action.Invoke();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Error in BlameableLock:");
                Debug.WriteLine(ex);
                Debug.WriteLine("BLTHR: " + _lockingThread?.Name);
                _lockingThread = null;
                return default;
            }
            finally
            {
                if (Monitor.IsEntered(_lock))
                {
                    _lockingThread = null;
                    Monitor.Exit(_lock);
                }
            }
        }

        public void TryBreakLock(bool yield = false, bool blockOverride = false)
        {
            if (_nonBlocking && !blockOverride) //do not break non-blocking threads
                return;
            _lockingThread?.Interrupt();
            if (yield)
                _lockingThread?.Join();
        }

        private void Enter()
        {
            var locked = false;
            while (!locked)
            {
                //keep trying to get the lock, and warn if not accessible after timeout
                locked = Monitor.TryEnter(_lock, 5000);
            }
        }
    }
}