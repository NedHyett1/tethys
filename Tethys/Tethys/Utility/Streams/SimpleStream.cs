﻿using System;
using System.IO;
using System.Text;

namespace Tethys.Utility.Streams
{
    /// <summary>
    /// Very simple binary stream that does not cache any bytes while reading, meaning bytes cannot be lost if you close
    /// this stream and assign another one. This is not a stream but a stream adapter.
    /// </summary>
    public class SimpleStream : IDisposable
    {
        private readonly Stream _delegate;
        private readonly bool _leaveOpen;
        private readonly Encoding _stringEncoding;

        public SimpleStream(Stream delegateStream, bool leaveOpen = false, Encoding stringEncoding = null)
        {
            _delegate = delegateStream;
            _leaveOpen = leaveOpen;
            _stringEncoding = stringEncoding ?? Encoding.UTF8;
        }

        /// <summary>
        /// Read a UTF-8 string from the stream.
        /// </summary>
        /// <returns></returns>
        public string ReadString()
        {
            var len = ReadInt();
            var bytes = _delegate.ReadExactly(len);
            return _stringEncoding.GetString(bytes);
        }

        /// <summary>
        /// Writes a UTF-8 string to the stream.
        /// </summary>
        /// <param name="str"></param>
        public void Write(string str)
        {
            var b = _stringEncoding.GetBytes(str);
            Write(b.Length);
            Write(b);
        }

        /// <summary>
        /// Reads a 32-bit int from the stream.
        /// </summary>
        /// <returns></returns>
        public int ReadInt()
        {
            var bytes = _delegate.ReadExactly(4);
            return (int)bytes[0] | (int)bytes[1] << 8 | (int)bytes[2] << 16 | (int)bytes[3] << 24;
        }

        /// <summary>
        /// Writes a 32-bit in to the stream.
        /// </summary>
        /// <param name="value"></param>
        public void Write(int value)
        {
            var buffer = new byte[4];
            buffer[0] = (byte)value;
            buffer[1] = (byte)(value >> 8);
            buffer[2] = (byte)(value >> 16);
            buffer[3] = (byte)(value >> 24);
            Write(buffer);
        }

        public uint ReadUInt()
        {
            var b = _delegate.ReadExactly(4);
            return BitConverter.ToUInt32(b, 0);
        }

        public void Write(uint value)
        {
            Write(BitConverter.GetBytes(value));
        }

        /// <summary>
        /// Reads EXACTLY the specified number of bytes from the stream.
        /// </summary>
        /// <param name="len"></param>
        /// <returns></returns>
        public byte[] ReadBytes(int len)
        {
            return _delegate.ReadExactly(len);
        }

        /// <summary>
        /// Read a byte array from the stream where the stream specifies the length.
        /// </summary>
        /// <returns></returns>
        public byte[] ReadTrackedBytes()
        {
            var len = ReadInt();
            return _delegate.ReadExactly(len);
        }

        /// <summary>
        /// Write all bytes in an array to the stream.
        /// </summary>
        /// <param name="bytes"></param>
        public void Write(byte[] bytes)
        {
            _delegate.Write(bytes, 0, bytes.Length);
        }

        /// <summary>
        /// Write the length of the array and the bytes to the stream.
        /// </summary>
        /// <param name="bytes"></param>
        public void WriteTrackedBytes(byte[] bytes)
        {
            Write(bytes.Length);
            Write(bytes);
        }

        public void Write(bool val)
        {
            _delegate.WriteByte((byte)(val ? 1 : 0));
        }

        public bool ReadBool()
        {
            return _delegate.ReadExactly(1)[0] == (byte)1;
        }

        public void Write(char c)
        {
            Write((int)c);
        }

        public char ReadChar()
        {
            return (char)ReadInt();
        }

        public void Write(short s)
        {
            var b = new byte[2];
            b[1] = (byte)(s >> 8);
            b[0] = (byte)(s & 255);
            Write(b);
        }

        public short ReadShort()
        {
            var b = _delegate.ReadExactly(2);
            return (short)((b[1] << 8) + b[0]);
        }

        public void Write(ushort s)
        {
            Write(BitConverter.GetBytes(s));
        }

        public ushort ReadUShort()
        {
            var b = _delegate.ReadExactly(2);
            return BitConverter.ToUInt16(b);
        }

        public void Write(long l)
        {
            Write(BitConverter.GetBytes(l));
        }

        public long ReadLong()
        {
            var b = _delegate.ReadExactly(8);
            return BitConverter.ToInt64(b, 0);
        }

        public void Write(ulong l)
        {
            Write(BitConverter.GetBytes(l));
        }

        public ulong ReadUlong()
        {
            var b = _delegate.ReadExactly(8);
            return BitConverter.ToUInt64(b, 0);
        }

        public void Write(float f)
        {
            Write(BitConverter.GetBytes(f));
        }

        public float ReadFloat()
        {
            var b = _delegate.ReadExactly(4);
            return BitConverter.ToSingle(b, 0);
        }

        public void Write(double d)
        {
            Write(BitConverter.GetBytes(d));
        }

        public double ReadDouble()
        {
            var b = _delegate.ReadExactly(8);
            return BitConverter.ToDouble(b, 0);
        }

        public void Write(decimal d)
        {
            foreach (var bit in decimal.GetBits(d))
            {
                Write(bit);
            }
        }

        public decimal ReadDecimal()
        {
            var bits = new int[4];
            for (var i = 0; i < 4; i++)
                bits[i] = ReadInt();
            return new decimal(bits);
        }

        public void Flush()
        {
            _delegate.Flush();
        }

        public void Dispose()
        {
            if (_leaveOpen)
                return;
            _delegate.Dispose();
        }
    }
}