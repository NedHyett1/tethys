﻿using System.Threading;

namespace Tethys.Utility
{
    public static class StreamExtensions
    {
        public static byte[] ReadExactly(this System.IO.Stream stream, int count)
        {
            var buffer = new byte[count];
            var offset = 0;
            while (offset < count)
            {
                var read = stream.Read(buffer, offset, count - offset);
                if (read == 0)
                    Thread.Sleep(50);
                offset += read;
            }
            System.Diagnostics.Debug.Assert(offset == count);
            return buffer;
        }
    }
}