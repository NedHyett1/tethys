﻿using System.IO;
using System.IO.Compression;

namespace Tethys
{
    /// <summary>
    /// Compression utility methods
    /// </summary>
    public static class TethysCompression
    {
        /// <summary>
        /// Compress an array of bytes down as much as possible.
        /// </summary>
        /// <param name="data">input data</param>
        /// <returns>Compressed version of input data</returns>
        public static byte[] Compress(this byte[] data)
        {
            var output = new MemoryStream();
            using (var dStream = new DeflateStream(output, CompressionLevel.Optimal))
                dStream.Write(data, 0, data.Length);
            return output.ToArray();
        }

        /// <summary>
        /// Decompress an array of bytes that was previously compressed.
        /// </summary>
        /// <param name="data">input data</param>
        /// <returns>The original data that has been decompressed</returns>
        public static byte[] Decompress(this byte[] data)
        {
            var input = new MemoryStream(data);
            var output = new MemoryStream();
            using (var dStream = new DeflateStream(input, CompressionMode.Decompress))
                dStream.CopyTo(output);
            return output.ToArray();
        }
    }
}