﻿using System;
using System.IO;
using System.Net.Sockets;
using System.Threading.Tasks;

using Tethys.BinaryAsideStream.Packet;
using Tethys.Header;
using Tethys.Packet;
using Tethys.PacketEncoding;
using Tethys.Sockets;
using Tethys.Sockets.SocketAdapters;
using Tethys.Utility;

namespace Tethys.Client
{
    /// <summary>
    /// The basis of all Tethys clients.
    /// </summary>
    public class TethysClient : TethysConnection
    {
        /// <summary>
        /// Create a new TethysClient using a default <see cref="TcpSocketAdapter"/>
        /// </summary>
        /// <param name="target">The address of the target server</param>
        /// <param name="port">The port of the target server</param>
        /// <param name="connectionId">An optional static connection ID to identify to the server with</param>
        /// <param name="packetEncoder"></param>
        /// <param name="protocolDescriptorBlock"></param>
        public TethysClient(string target, int port, Guid? connectionId = null, IPacketEncoder packetEncoder = null, TethysProtocolDescriptorBlock protocolDescriptorBlock = null) : this(new TcpSocketAdapter(target, port), connectionId, packetEncoder, protocolDescriptorBlock)
        {
        }

        /// <summary>
        /// Create a new TethysClient using a custom <see cref="ITethysSocketAdapter"/>
        /// </summary>
        /// <param name="socket">The socket to use to connect to the server</param>
        /// <param name="connectionId">An optional static connection ID to identify to the server with</param>
        /// <param name="packetEncoder"></param>
        /// <param name="protocolDescriptorBlock"></param>
        public TethysClient(ITethysSocketAdapter socket, Guid? connectionId = null, IPacketEncoder packetEncoder = null, TethysProtocolDescriptorBlock protocolDescriptorBlock = null) : base(socket, connectionId, packetEncoder, protocolDescriptorBlock)
        {
            ReadEvent += OnReadEvent;
            IoFailure += OnIoFailure;
        }

        private void OnIoFailure(object sender, Exception e)
        {
            //TODO: do something here?
        }

        private void OnReadEvent(object sender, TethysPacket e)
        {
            Task.CompletedTask.ContinueWith(t => e.ProcessPacketClient(this));
        }

        /// <summary>
        /// Request a full duplex binary stream from the server that exists outside of the standard
        /// packet frame model. All transmissions on this stream are unmanaged by Tethys and is the
        /// responsibility of the invoker to handle.
        /// </summary>
        /// <param name="protocol"></param>
        /// <returns></returns>
        public Task<Stream> RequestBinaryAsideStream(string protocol)
        {
            return Task.CompletedTask.ContinueWith(t =>
            {
                var packet = new RequestBinaryAsideStreamPacket(protocol);
                var response = this.WriteAndAwaitResponseAsync<EstablishBinaryAsideStreamPacket>(packet).GetAwaiter().GetResult();
                var currentEndPoint = SocketAdapter.GetRemoteAddress();
                var newClient = new TcpClient();
                newClient.Connect(currentEndPoint);
                var writer = new BinaryWriter(newClient.GetStream());
                writer.Write(EnumProtocol.Raw.ToString());
                writer.Write(response.ConnectionIdentifier.ToString());
                writer.Flush();
                return (Stream)newClient.GetStream();
            });
        }
    }
}