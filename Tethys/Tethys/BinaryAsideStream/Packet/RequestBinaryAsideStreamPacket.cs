﻿using System;

using Tethys.Client;
using Tethys.Packet;
using Tethys.Server;

namespace Tethys.BinaryAsideStream.Packet
{
    /// <summary>
    /// Sent by the client to request the server to initiate a binary stream.
    /// </summary>
    [Serializable]
    [TethysPacket(TethysSide.Server)]
    public class RequestBinaryAsideStreamPacket : TethysPacket
    {
        public string Protocol { get; private set; }

        public RequestBinaryAsideStreamPacket()
        {
        }

        public RequestBinaryAsideStreamPacket(string protocol)
        {
            Protocol = protocol;
        }

        public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
        {
            var channelId = server.PrepareForBinaryAsideStream(Protocol, false);
            Reply(new EstablishBinaryAsideStreamPacket(channelId, Protocol));
        }

        public override void ProcessPacketClient(TethysClient client)
        {
            throw new NotImplementedException();
        }
    }
}