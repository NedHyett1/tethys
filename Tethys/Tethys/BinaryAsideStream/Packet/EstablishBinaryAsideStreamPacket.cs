﻿using System;
using System.IO;

using Tethys.Client;
using Tethys.Packet;
using Tethys.Server;
using Tethys.Sockets.SocketAdapters;

namespace Tethys.BinaryAsideStream.Packet
{
    /// <summary>
    /// Performs the handshake for the stream, informing the client of the identifier.
    /// When used in a server-invoke, will invoke the handler on the client.
    /// </summary>
    [Serializable]
    public class EstablishBinaryAsideStreamPacket : TethysPacket
    {
        public Guid ConnectionIdentifier { get; private set; }
        public string Protocol { get; private set; }

        public EstablishBinaryAsideStreamPacket()
        {
        }

        public EstablishBinaryAsideStreamPacket(Guid connectionIdentifier, string protocol)
        {
            ConnectionIdentifier = connectionIdentifier;
            Protocol = protocol;
        }

        public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
        {
        }

        public override void ProcessPacketClient(TethysClient client)
        {
            var currentEndPoint = client.SocketAdapter.GetRemoteAddress();
            var newClient = new TcpSocketAdapter(currentEndPoint);
            newClient.Connect();
            var writer = new BinaryWriter(newClient.GetStream());
            writer.Write("TETHYS-BINARY");
            writer.Write(ConnectionIdentifier.ToString());
            writer.Flush();
            BinaryAsideStreamProtocolRegistry.ProcessBinaryStream(Protocol, ConnectionIdentifier, newClient);
        }
    }
}