﻿using System.IO;

namespace Tethys.BinaryAsideStream
{
    /// <summary>
    /// Handles invocation of the BinaryAsideStream.
    /// </summary>
    public interface IBinaryAsideStreamProtocol
    {
        /// <summary>
        /// Called by the registry when the remote client/server has invoked a stream on this protocol.
        /// </summary>
        /// <param name="stream"></param>
        void HandleRemote(Stream stream);

        void HandleLocal(Stream stream);
    }
}