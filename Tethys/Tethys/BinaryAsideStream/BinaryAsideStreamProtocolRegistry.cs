﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

using Tethys.Sockets;
using Tethys.Utility;

namespace Tethys.BinaryAsideStream
{
    /// <summary>
    /// Handles BinaryAsideStream protocols.
    /// </summary>
    public static class BinaryAsideStreamProtocolRegistry
    {
        private static readonly Dictionary<string, IBinaryAsideStreamProtocol> Protocols = new Dictionary<string, IBinaryAsideStreamProtocol>();
        private static readonly Dictionary<Guid, SynchronisedExchangeLock<ITethysSocketAdapter>> AwaitedConnections = new Dictionary<Guid, SynchronisedExchangeLock<ITethysSocketAdapter>>();

        /// <summary>
        /// Register a custom BinaryAsideProtocol.
        /// </summary>
        /// <param name="protocolId"></param>
        /// <param name="protocol"></param>
        public static void RegisterProtocol(string protocolId, IBinaryAsideStreamProtocol protocol)
        {
            Protocols.Add(protocolId, protocol);
        }

        /// <summary>
        /// Used internally to process the binary stream.
        /// Invoked when the remote connects to the local in order to start an async process that feeds the stream with data or
        /// constantly reads data from the stream.
        ///
        /// The protocol is ALWAYS wrapped into an async task, so do not expect to run inside the context of the caller.
        /// </summary>
        /// <param name="protocolId"></param>
        /// <param name="connectionId"></param>
        /// <param name="client"></param>
        /// <returns></returns>
        internal static bool ProcessBinaryStream(string protocolId, Guid connectionId, ITethysSocketAdapter client)
        {
            if (!Protocols.ContainsKey(protocolId))
                return false;
            var protocol = Protocols[protocolId];
            Task.CompletedTask.ContinueWith(t => { protocol.HandleRemote(client.GetStream()); });
            return true;
        }

        /// <summary>
        /// Used internally by the server to await a client trying to connect with a BinaryAside stream.
        /// If the remote to does not respond in 90 seconds, result is null.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        internal static Task<Stream> AwaitClientBinaryAsideConnection(Guid id)
        {
            return Task.CompletedTask.ContinueWith(t =>
            {
                var cont = new SynchronisedExchangeLock<ITethysSocketAdapter>();
                lock (AwaitedConnections)
                {
                    AwaitedConnections.Add(id, cont);
                }

                var client = cont.Get(TimeSpan.FromSeconds(90));

                lock (AwaitedConnections)
                {
                    AwaitedConnections.Remove(id);
                }

                return (Stream)client?.GetStream();
            });
        }

        /// <summary>
        /// Used internally by the server to notify the registry that a connection has been made.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="tcpClient"></param>
        internal static void ProvideBinaryAsideConnection(Guid id, ITethysSocketAdapter tcpClient)
        {
            lock (AwaitedConnections)
            {
                AwaitedConnections[id].Give(tcpClient);
            }
        }
    }
}