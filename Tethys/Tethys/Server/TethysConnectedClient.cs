﻿using System;
using System.IO;
using System.Threading.Tasks;

using Tethys.BinaryAsideStream;
using Tethys.BinaryAsideStream.Packet;
using Tethys.Packet;
using Tethys.Sockets;

namespace Tethys.Server
{
    /// <summary>
    /// The basic server representation of a client. Extend this to store your own data in the client.
    /// </summary>
    public class TethysConnectedClient : TethysConnection
    {
        /// <summary>
        /// The server that this ConnectedClient belongs to.
        /// </summary>
        public readonly ITethysServer Server;

        /// <summary>
        /// Create a new <see cref="TethysConnectedClient"/>
        /// </summary>
        /// <param name="server"></param>
        /// <param name="socket"></param>
        public TethysConnectedClient(ITethysServer server, ITethysSocketAdapter socket) : base(socket, packetEncoder: server.GetPacketEncoder(), sslConnectionProperties: server.GetSslConnectionProperties())
        {
            Server = server;

            ReadEvent += OnReadEvent;
            IoFailure += OnIoFailure;
        }

        private void OnIoFailure(object sender, Exception e)
        {
            Server.DisconnectClient(ConnectionId, e.Message);
        }

        private void OnReadEvent(object sender, TethysPacket e)
        {
            Task.CompletedTask.ContinueWith(t => e.ProcessPacketServer(Server, this));
        }

        /// <summary>
        /// Request a full duplex binary stream from the client that exists outside of the standard
        /// packet frame model. All transmissions on this stream are unmanaged by Tethys and is the
        /// responsibility of the invoker to handle.
        /// </summary>
        /// <param name="protocol">the protocol handler to invoke on the client</param>
        /// <returns>A stream leading directly to that protocol handler</returns>
        public Task<Stream> RequestBinaryAsideStream(string protocol)
        {
            return Task.CompletedTask.ContinueWith(t =>
            {
                var connId = Server.PrepareForBinaryAsideStream(protocol, true);
                var packet = new EstablishBinaryAsideStreamPacket(connId, protocol);
                var connectionTask = BinaryAsideStreamProtocolRegistry.AwaitClientBinaryAsideConnection(connId);
                WritePacket(packet);
                var stream = connectionTask.GetAwaiter().GetResult();
                return stream;
            });
        }
    }
}