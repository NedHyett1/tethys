﻿using System;

using Tethys.PacketEncoding;
using Tethys.Security;
using Tethys.Sockets;

namespace Tethys.Server
{
    /// <summary>
    /// Basic Tethys Server; accepts all clients and uses basic <see cref="TethysConnectedClient"/> objects.
    /// </summary>
    public class TethysServer : TethysServerBase
    {
        public TethysServer(int port, Guid? connectionId = null, IPacketEncoder packetEncoder = null, SSLContainer sslContainer = null, ITethysListenerAdapter listenerAdapter = null) : base(port, connectionId, packetEncoder, sslContainer, listenerAdapter)
        {
        }

        /// <summary>
        /// Build a basic <see cref="TethysConnectedClient"/>
        /// </summary>
        /// <param name="clientAdapter">the client that was caught by the base server</param>
        /// <returns>the TCC object</returns>
        protected override TethysConnectedClient BuildClientObject(ITethysSocketAdapter clientAdapter)
        {
            return new TethysConnectedClient(this, clientAdapter);
        }

        /// <summary>
        /// Verify that a client is allowed to connect to this server.
        /// </summary>
        /// <param name="tethysClient">the client trying to connect</param>
        /// <returns>true to allow, false to block</returns>
        protected override bool VerifyClient(TethysConnectedClient tethysClient)
        {
            return true;
        }
    }
}