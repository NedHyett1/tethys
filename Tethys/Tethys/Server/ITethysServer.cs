﻿using System;
using System.Threading.Tasks;

using Tethys.Packet;
using Tethys.PacketEncoding;
using Tethys.Security;

namespace Tethys.Server
{
    /// <summary>
    /// Base interface for public facing TethysServer functions.
    /// </summary>
    public interface ITethysServer
    {
        /// <summary>
        /// Gets the connection identifier for this server.
        /// </summary>
        /// <returns></returns>
        Guid GetConnectionId();

        /// <summary>
        /// Broadcast a packet to all connected clients.
        /// </summary>
        /// <param name="packet">the packet to broadcast</param>
        void Broadcast(TethysPacket packet);

        /// <summary>
        /// Broadcast a packet to all connected clients asynchronously.
        /// </summary>
        /// <param name="packet">the packet to broadcast</param>
        /// <returns>a task that will complete when the broadcast is finished.</returns>
        Task BroadcastAsync(TethysPacket packet);

        /// <summary>
        /// Broadcast a packet to all connected clients, await their responses and return an array of all responses organised by client ID.
        /// </summary>
        /// <typeparam name="TPacket">the packet type expected as a response</typeparam>
        /// <param name="packet">The packet to broadcast</param>
        /// <returns>An array of all responses organised by client ID.</returns>
        Task<(Guid, TPacket)[]> BroadcastAndAwaitResponseAsync<TPacket>(TethysPacket packet) where TPacket : TethysPacket;

        /// <summary>
        /// Get a client by the client ID.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        TethysConnectedClient GetClient(Guid clientId);

        /// <summary>
        /// Send a disconnection packet and force the client to disconnect.
        /// </summary>
        /// <param name="clientId">the ID of the client to disconnect</param>
        /// <param name="reason">the reason why the client was disconnected</param>
        void DisconnectClient(Guid clientId, string reason = null);

        /// <summary>
        /// Get the transport service used to encode and decode the protocol.
        /// </summary>
        /// <returns></returns>
        IPacketEncoder GetPacketEncoder();

        /// <summary>
        /// Change the transport service used to encode and decode the protocol.
        /// </summary>
        /// <param name="service"></param>
        void SetTransportService(IPacketEncoder service);

        /// <summary>
        /// Prepares the server to accept a binary aside stream.
        /// </summary>
        /// <param name="protocol">the protocol handler to invoke on the client</param>
        /// <param name="side">identifies the initiator: true for server, false for client</param>
        /// <returns></returns>
        Guid PrepareForBinaryAsideStream(string protocol, bool side);

        SSLContainer GetSslConnectionProperties();

        void Start();

        void Stop();
    }
}