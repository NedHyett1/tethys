﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Tethys.BinaryAsideStream;
using Tethys.Packet;
using Tethys.PacketEncoding;
using Tethys.Security;
using Tethys.Sockets;

namespace Tethys.Server
{
    /// <summary>
    /// The base class for all Tethys servers.
    /// </summary>
    public abstract class TethysServerBase : ITethysServer, IDisposable
    {
        /// <summary>
        /// Fired when a client has been connected to the server.
        /// </summary>
        public event EventHandler<TethysConnectedClient> OnClientConnected;

        /// <summary>
        /// Fired when a client has disconnected from the server. Don't async this or you'll get a disposed client.
        /// </summary>
        public event EventHandler<(TethysConnectedClient, string)> OnClientDisconnected;

        /// <summary>
        /// A list of all currently connected clients.
        /// </summary>
        private readonly ConcurrentDictionary<Guid, TethysConnectedClient> _clients = new ConcurrentDictionary<Guid, TethysConnectedClient>();

        /// <summary>
        /// Get the number of currently connected clients
        /// </summary>
        public int NumberOfClients => _clients.Count;

        /// <summary>
        /// Get a list of all currently connected client IDs.
        /// </summary>
        public List<Guid> ClientIds => _clients.Keys.ToList();

        /// <summary>
        /// The ID of the server shown to clients.
        /// </summary>
        public Guid ConnectionId { get; }

        /// <summary>
        /// The service used to encode and decode data to and from bytes.
        /// </summary>
        private IPacketEncoder PacketEncoder { get; set; }

        /// <summary>
        /// A list of BinaryAside requests that are awaiting connection.
        /// </summary>
        private List<(Guid, string, bool)> AwaitingBinaryAsideStreams { get; } = new List<(Guid, string, bool)>();

        public SSLContainer SslConnectionProperties { get; }

        public TethysInboundConnectionListener InboundConnectionListener { get; }

        /// <summary>
        /// Create a new Tethys Server.
        /// </summary>
        /// <param name="port">The port to operate on</param>
        /// <param name="connectionId">The server ID for static ID servers, null to automatically generate</param>
        /// <param name="packetEncoder">The transport service to use</param>
        /// <param name="sslConnectionProperties"></param>
        /// <param name="listenerAdapter"></param>
        protected TethysServerBase(int port, Guid? connectionId = null, IPacketEncoder packetEncoder = null, SSLContainer sslConnectionProperties = null, ITethysListenerAdapter listenerAdapter = null)
        {
            //If we aren't provided a connection ID, generate our own.
            if (!connectionId.HasValue)
                connectionId = Guid.NewGuid();

            ConnectionId = connectionId.Value;

            PacketEncoder = packetEncoder ?? new DotNetBinaryPacketEncoder();

            SslConnectionProperties = sslConnectionProperties;

            InboundConnectionListener = listenerAdapter == null ? new TethysInboundConnectionListener(port) : new TethysInboundConnectionListener(listenerAdapter);
            InboundConnectionListener.ClientConnected += AddClient;
            InboundConnectionListener.BinaryStreamClientConnected += AddBinaryAsideClient;
        }

        /// <summary>
        /// Fired when a client connects with a BinaryAside intent.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddBinaryAsideClient(object sender, (ITethysSocketAdapter, Guid) e)
        {
            lock (AwaitingBinaryAsideStreams)
            {
                if (AwaitingBinaryAsideStreams.All(abs => abs.Item1 != e.Item2))
                    return;
                var awaiting = AwaitingBinaryAsideStreams.FirstOrDefault(abs => abs.Item1 == e.Item2);
                AwaitingBinaryAsideStreams.Remove(awaiting);

                if (awaiting.Item3)
                {
                    BinaryAsideStreamProtocolRegistry.ProvideBinaryAsideConnection(e.Item2, e.Item1);
                }
                else
                {
                    BinaryAsideStreamProtocolRegistry.ProcessBinaryStream(awaiting.Item2, awaiting.Item1, e.Item1);
                }
            }
        }

        /// <summary>
        /// Override to use a custom client object in order to store extra data with each client. Note that if a client is disconnected then
        /// all data stored with it will be lost.
        /// </summary>
        /// <param name="clientAdapter"></param>
        /// <returns></returns>
        protected abstract TethysConnectedClient BuildClientObject(ITethysSocketAdapter clientAdapter);

        /// <summary>
        /// Override to decide what clients to allow into a server. e.g. whitelist
        /// </summary>
        /// <param name="tethysClient">The client trying to join the server</param>
        /// <returns>true to allow, false to block</returns>
        protected abstract bool VerifyClient(TethysConnectedClient tethysClient);

        /// <summary>
        /// Adds a client to the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddClient(object sender, ITethysSocketAdapter e)
        {
            //The client object
            var tClient = BuildClientObject(e);

            //Ask the client who they are TODO: move this into the descriptor block
            var iam = tClient.WriteAndAwaitResponseAsync<TethysIdentifyPacket>(new TethysIdentifyPacket()).GetAwaiter().GetResult();
            tClient.ConnectionId = iam.Id;

            //Verify that the client is allowed to connect
            if (!VerifyClient(tClient))
            {
                tClient.Disconnect("You are not allowed to connect to this server.");
                tClient.Dispose();
                return;
            }

            //Add them to the client list
            _clients.TryAdd(tClient.ConnectionId, tClient);
            //Raise an event to tell the rest of the server that we got a new client
            OnClientConnectedInvoke(tClient);
        }

        /// <summary>
        /// Invoke the client connect event.
        /// </summary>
        /// <param name="client"></param>
        private void OnClientConnectedInvoke(TethysConnectedClient client)
        {
            OnClientConnected?.Invoke(this, client);
        }

        /// <summary>
        /// Invoke the client disconnect event.
        /// </summary>
        /// <param name="client"></param>
        /// <param name="reason"></param>
        private void OnClientDisconnectedInvoke(TethysConnectedClient client, string reason)
        {
            OnClientDisconnected?.Invoke(this, (client, reason));
        }

        public Guid GetConnectionId()
        {
            return ConnectionId;
        }

        /// <summary>
        /// Broadcast a packet to all connected clients.
        /// </summary>
        /// <param name="packet">the packet to broadcast</param>
        public void Broadcast(TethysPacket packet)
        {
            var tasks = new List<Task>();
            foreach (var client in _clients) tasks.Add(client.Value.WritePacketAsync(packet));
            Task.WhenAll(tasks).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Broadcast a packet to all connected clients asynchronously.
        /// </summary>
        /// <param name="packet">the packet to broadcast</param>
        /// <returns>a task that will complete when the broadcast is finished.</returns>
        public Task BroadcastAsync(TethysPacket packet)
        {
            return Task.CompletedTask.ContinueWith(t => { Broadcast(packet); });
        }

        /// <summary>
        /// Broadcast a packet to all connected clients, await their responses and return an array of all responses organised by client ID.
        /// </summary>
        /// <typeparam name="TPacket">the packet type expected as a response</typeparam>
        /// <param name="packet">The packet to broadcast</param>
        /// <returns>An array of all responses organised by client ID.</returns>
        public Task<(Guid, TPacket)[]> BroadcastAndAwaitResponseAsync<TPacket>(TethysPacket packet) where TPacket : TethysPacket
        {
            var tasks = new List<Task<(Guid, TPacket)>>();
            foreach (var client in _clients)
            {
                tasks.Add(Task.CompletedTask.ContinueWith(t =>
                {
                    var clientId = client.Value.ConnectionId;
                    var response = client.Value.WriteAndAwaitResponseAsync<TPacket>(packet).GetAwaiter().GetResult();
                    return (clientId, response);
                }));
            }

            return Task.WhenAll(tasks);
        }

        /// <summary>
        /// Internal function to remove a disconnected client.
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="reason"></param>
        public void DisconnectClient(Guid clientId, string reason = null)
        {
            _clients.TryRemove(clientId, out var ret);
            OnClientDisconnectedInvoke(ret, reason);
            ret.Dispose();
        }

        /// <summary>
        /// Get the current transport service used to encode and decode packets.
        /// </summary>
        /// <returns></returns>
        public IPacketEncoder GetPacketEncoder()
        {
            return PacketEncoder;
        }

        /// <summary>
        /// Set the current transport service used to encode and decode packets.
        /// </summary>
        /// <param name="service"></param>
        public void SetTransportService(IPacketEncoder service)
        {
            PacketEncoder = service;
        }

        public Guid PrepareForBinaryAsideStream(string protocol, bool side)
        {
            lock (AwaitingBinaryAsideStreams)
            {
                var connId = Guid.NewGuid();
                AwaitingBinaryAsideStreams.Add((connId, protocol, side));
                return connId;
            }
        }

        public SSLContainer GetSslConnectionProperties()
        {
            return SslConnectionProperties;
        }

        public void Start()
        {
            InboundConnectionListener.Start();
        }

        public void Stop()
        {
            InboundConnectionListener.Dispose();
        }

        /// <summary>
        /// Get a client by the client ID.
        /// </summary>
        /// <param name="clientId"></param>
        /// <returns></returns>
        public TethysConnectedClient GetClient(Guid clientId)
        {
            return _clients[clientId];
        }

        public void Dispose()
        {
            foreach (var clientId in _clients.Keys)
            {
                DisconnectClient(clientId, "Server Shutting Down");
            }
        }
    }
}