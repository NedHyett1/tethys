﻿using System;
using System.Net;
using System.Threading;
using System.Threading.Tasks;

using Tethys.Sockets;
using Tethys.Sockets.Listener;
using Tethys.Utility;
using Tethys.Utility.Streams;

namespace Tethys.Server
{
    /// <summary>
    /// Awaits inbound connections from a TcpListener and pushes them as events to subscribed listeners.
    /// </summary>
    public sealed class TethysInboundConnectionListener : IDisposable
    {
        /// <summary>
        /// Fired when a new client has connected to the socket.
        /// </summary>
        public event EventHandler<ITethysSocketAdapter> ClientConnected;

        /// <summary>
        /// Fired when a binary stream client has connected.
        /// </summary>
        public event EventHandler<(ITethysSocketAdapter, Guid)> BinaryStreamClientConnected;

        /// <summary>
        /// The server socket listener that is awaiting connections
        /// </summary>
        private readonly ITethysListenerAdapter _serverSocket;

        /// <summary>
        /// The thread that runs to async check for connections.
        /// </summary>
        private readonly Thread _acceptThread;

        private readonly CancellationTokenSource _cancellationTokenSource;
        private readonly CancellationToken _cancellationToken;

        /// <summary>
        /// Create a <see cref="TethysInboundConnectionListener"/> using the specified <see cref="ITethysListenerAdapter"/>
        /// </summary>
        /// <param name="listener"></param>
        public TethysInboundConnectionListener(ITethysListenerAdapter listener)
        {
            _serverSocket = listener;
            _acceptThread = new Thread(AcceptClient)
            {
                IsBackground = true
            };
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
        }

        /// <summary>
        /// Create a new <see cref="TethysInboundConnectionListener"/> defaulting to a <see cref="TcpListenerAdapter"/>
        /// </summary>
        /// <param name="port">the port to listen on</param>
        /// <param name="listeningAddress">the address to listen on</param>
        public TethysInboundConnectionListener(int port, IPAddress listeningAddress = null)
        {
            _serverSocket = new TcpListenerAdapter(port, listeningAddress ?? IPAddress.Any);
            _acceptThread = new Thread(AcceptClient)
            {
                IsBackground = true
            };
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
        }

        /// <summary>
        /// Thread body to accept a client. Dies on cancellation token use.
        /// </summary>
        private void AcceptClient()
        {
            while (!_cancellationToken.IsCancellationRequested)
            {
                var client = _serverSocket.AcceptClient();
                string type;
                Guid id;

                //Read out the connection type magic from the initialiser
                using (var reader = new SimpleStream(client.GetStream(), true))
                {
                    type = reader.ReadString();
                    id = Guid.Parse(reader.ReadString());
                }

                switch (Enum.Parse<EnumProtocol>(type))
                {
                    //Standard Tethys stream
                    case EnumProtocol.TethysFrame:
                        Task.CompletedTask.ContinueWith(t1 =>
                        {
                            ClientConnected?.Invoke(this, client);
                        }, _cancellationToken);
                        break;
                    //Binary aside stream
                    case EnumProtocol.Raw:
                        Task.CompletedTask.ContinueWith(t1 =>
                        {
                            BinaryStreamClientConnected?.Invoke(this, (client, id));
                        }, _cancellationToken);
                        break;

                    default:
                        client.Dispose();
                        break;
                }
            }
        }

        /// <summary>
        /// Start listening for connections.
        /// </summary>
        public void Start()
        {
            _serverSocket.Start();
            _acceptThread.Start();
        }

        public void Dispose()
        {
            _serverSocket.Stop();
            _cancellationTokenSource.Cancel();
            _cancellationTokenSource.Dispose();
        }
    }
}