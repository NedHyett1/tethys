﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

using Tethys.Client;
using Tethys.Server;

namespace Tethys.Packet
{
    /// <summary>
    /// The basic Tethys Packet type. Extend this to make your own packets.
    /// </summary>
    [Serializable]
    public abstract class TethysPacket
    {
        /// <summary>
        /// The source of the packet, used to create replies.
        /// </summary>
        [NonSerialized] internal TethysConnection Source = null;

        /// <summary>
        /// The packet's ID tag, coordinates responses.
        /// </summary>
        public Guid? PacketId { get; set; }

        /// <summary>
        /// The packet's response ID tag, set to the PacketId of the packet that is being responded to.
        /// </summary>
        public Guid? ResponseId { get; set; }

        #region Abstract Handlers

        /// <summary>
        /// Process the packet on the server side.
        /// </summary>
        /// <param name="server">The server object</param>
        /// <param name="client">The client object that generated this packet</param>
        public abstract void ProcessPacketServer(ITethysServer server, TethysConnectedClient client);

        /// <summary>
        /// Process the packet on the client side.
        /// </summary>
        /// <param name="client">The client object</param>
        public abstract void ProcessPacketClient(TethysClient client);

        #endregion Abstract Handlers

        #region Reply Methods

        /// <summary>
        /// Reply to this packet immediately.
        /// </summary>
        /// <param name="packet">The packet to reply with</param>
        public void Reply(TethysPacket packet)
        {
            Debug.Assert(PacketId != null, nameof(PacketId) + " != null");
            packet.ResponseId = PacketId.Value;
            Source?.WritePacket(packet);
        }

        /// <summary>
        /// Reply to this packet asynchronously.
        /// </summary>
        /// <param name="packet">The packet to reply with</param>
        /// <returns>A task that will complete when the packet has been sent</returns>
        public Task ReplyAsync(TethysPacket packet)
        {
            Debug.Assert(PacketId != null, nameof(PacketId) + " != null");
            packet.ResponseId = PacketId.Value;
            return Source?.WritePacketAsync(packet);
        }

        /// <summary>
        /// Reply to this packet and await a response from the other side.
        /// </summary>
        /// <typeparam name="TPacket">The expected response packet type</typeparam>
        /// <param name="packet">The packet to reply with</param>
        /// <returns>A task that will complete when the other side has responded or the connection has timed out</returns>
        public Task<TPacket> ReplyAndAwaitResponseAsync<TPacket>(TethysPacket packet) where TPacket : TethysPacket
        {
            Debug.Assert(PacketId != null, nameof(PacketId) + " != null");
            packet.ResponseId = PacketId.Value;
            return Source?.WriteAndAwaitResponseAsync<TPacket>(packet);
        }

        #endregion Reply Methods
    }
}