﻿using System;

using Tethys.Client;
using Tethys.Server;

namespace Tethys.Packet
{
    /// <summary>
    /// Sent by the remote to notify the local that a disconnection is about to happen.
    /// When received by the server, the server begins the process of removing the client from records.
    /// When received by the client, the client disposes itself.
    /// </summary>
    [Serializable]
    public class TethysDisconnectPacket : TethysPacket
    {
        public string Reason { get; set; }

        public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
        {
            server.DisconnectClient(client.ConnectionId, Reason);
        }

        public override void ProcessPacketClient(TethysClient client)
        {
            client.Dispose();
        }
    }
}