using System;

using Tethys.Client;
using Tethys.Server;

namespace Tethys.Packet
{
    /// <summary>
    /// This packet forces the local to send their identity token back to the remote.
    /// </summary>
    [Serializable]
    internal class TethysIdentifyPacket : TethysPacket
    {
        /// <summary>
        /// The ID returned by the receiver.
        /// </summary>
        public Guid Id { get; set; }

        public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
        {
            Reply(new TethysIdentifyPacket
            {
                Id = server.GetConnectionId()
            });
        }

        public override void ProcessPacketClient(TethysClient client)
        {
            Reply(new TethysIdentifyPacket
            {
                Id = client.ConnectionId
            });
        }
    }
}