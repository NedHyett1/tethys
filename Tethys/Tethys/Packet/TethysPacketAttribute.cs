﻿using System;

namespace Tethys.Packet
{
    /// <summary>
    /// Provides metadata about a packet.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class TethysPacketAttribute : Attribute
    {
        public TethysSide CanProcessOn { get; }

        public TethysPacketAttribute(TethysSide canProcessOn = TethysSide.Both)
        {
            CanProcessOn = canProcessOn;
        }
    }
}