﻿using System.Security.Cryptography.X509Certificates;

namespace Tethys.Security
{
    public class SSLContainer
    {
        /// <summary>
        /// The certificate name
        /// </summary>
        public string CertificateName { get; }

        /// <summary>
        /// The certificate
        /// </summary>
        public X509Certificate Certificate { get; }

        /// <summary>
        /// Allow clients to connect with self-signed certificates
        /// </summary>
        public bool AllowSelfSigned { get; }

        public bool EnforceMutualAuthentication { get; }

        public bool ForceRevocationCheck { get; }

        public SSLContainer()
        {
        }

        public SSLContainer(string certName, X509Certificate cert, bool allowSelfSigned = false, bool enforceMutualAuthentication = false, bool forceRevocationCheck = false)
        {
            CertificateName = certName;
            Certificate = cert;
            AllowSelfSigned = allowSelfSigned;
            EnforceMutualAuthentication = enforceMutualAuthentication;
            ForceRevocationCheck = forceRevocationCheck;
        }
    }
}