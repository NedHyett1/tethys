﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Tethys.Client;
using Tethys.Header;
using Tethys.Packet;
using Tethys.PacketEncoding;
using Tethys.Security;
using Tethys.Server;
using Tethys.Sockets;
using Tethys.Utility;
using Tethys.Utility.Streams;

namespace Tethys
{
    /// <inheritdoc />
    /// <summary>
    /// Base class for all Tethys endpoints.
    /// </summary>
    public abstract class TethysConnection : IDisposable
    {
        private static readonly byte[] Magic = { 20, 5, 20, 8, 25, 19 }; //TODO: move to constants file

        #region Events

        /// <summary>
        /// Fired when a packet that is not a response packet is read from the data stream.
        /// </summary>
        protected event EventHandler<TethysPacket> ReadEvent;

        /// <summary>
        /// Fired when an IO failure is encountered. May trigger disconnect.
        /// </summary>
        protected event EventHandler<Exception> IoFailure;

        #endregion Events

        /// <summary>
        /// The input stream from the socket.
        /// </summary>
        private SimpleStream _inputStream;

        /// <summary>
        /// The output stream from the socket.
        /// </summary>
        private SimpleStream _outputStream;

        /// <summary>
        /// A list of latches that are currently awaiting responses from the other side of the connection.
        /// One latch per packet, sorted by packet ID.
        /// </summary>
        private readonly Dictionary<Guid, TaskCompletionSource<TethysPacket>> _responseLatches = new Dictionary<Guid, TaskCompletionSource<TethysPacket>>();

        /// <summary>
        /// The SocketAdapter that is controlling this side of the connection.
        /// </summary>
        public ITethysSocketAdapter SocketAdapter { get; }

        /// <summary>
        /// The connection ID with which to identify to the other side of the connection with.
        /// </summary>
        public Guid ConnectionId { get; internal set; }

        /// <summary>
        /// The IP address of the other side of the connection.
        /// </summary>
        public IPAddress IpAddress => SocketAdapter.GetRemoteAddress().Address;

        /// <summary>
        /// The transport service to use for this connection.
        /// </summary>
        private IPacketEncoder PacketEncoder { get; }

        /// <summary>
        /// Dirty flag to signal to all related threads that they are to die immediately. TODO: replace with cancellation token?
        /// </summary>
        private bool _die;

        /// <summary>
        /// Write calls lock this object in order to run synchronously.
        /// </summary>
        private readonly object _writeLockHandle = new object();

        /// <summary>
        /// The thread currently performing read operations.
        /// </summary>
        private Thread _readThread;

        /// <summary>
        /// The current setting for the SSL connection. Used to provide server-client encryption
        /// </summary>
        public SSLContainer SslConnectionProperties { get; }

        public TethysProtocolDescriptorBlock ProtocolDescriptorBlock { get; }

        /// <summary>
        /// Create a new TethysConnection.
        /// </summary>
        /// <param name="socketAdapter">the tcp client to connect with</param>
        /// <param name="connectionId">An optional connection ID with which to identify to the server with.</param>
        /// <param name="packetEncoder">The transport service to use</param>
        protected TethysConnection(ITethysSocketAdapter socketAdapter, Guid? connectionId = null, IPacketEncoder packetEncoder = null, TethysProtocolDescriptorBlock protocolDescriptorBlock = null, SSLContainer sslConnectionProperties = null)
        {
            SocketAdapter = socketAdapter;

            SslConnectionProperties = sslConnectionProperties;

            PacketEncoder = packetEncoder ?? new DotNetBinaryPacketEncoder();

            //If the connection ID is not provided, generate one.
            if (!connectionId.HasValue)
                connectionId = Guid.NewGuid();

            ConnectionId = connectionId.Value;

            ProtocolDescriptorBlock = protocolDescriptorBlock ?? new TethysProtocolDescriptorBlock();

            var theStream = SocketAdapter.GetStream();
            try
            {
                if (SslConnectionProperties != null)
                {
                    if (this is TethysConnectedClient)
                    {
                        theStream = new SslStream(SocketAdapter.GetStream(),
                            false,
                            CertificateValidationCallback,
                            CertificateSelectionCallback);
                        ((SslStream)theStream).AuthenticateAsServer(SslConnectionProperties.Certificate, SslConnectionProperties.EnforceMutualAuthentication, SslProtocols.Tls12, SslConnectionProperties.ForceRevocationCheck);
                    }
                    else
                    {
                        var certs = new X509CertificateCollection();
                        if (SslConnectionProperties.Certificate != null)
                            certs.Add(SslConnectionProperties.Certificate);

                        theStream = new SslStream(SocketAdapter.GetStream(),
                            false,
                            CertificateValidationCallback,
                            CertificateSelectionCallback);

                        ((SslStream)theStream).AuthenticateAsClient(SslConnectionProperties.CertificateName, certs, SslProtocols.Tls12, SslConnectionProperties.ForceRevocationCheck);
                    }
                }
            }
            catch (AuthenticationException ex)
            {
                throw new Exception("Failed to connect with SSL. Check SSL configuration and try again.", ex);
            }

            var rawInput = new SimpleStream(theStream, true);
            var rawOutput = new SimpleStream(theStream, true);

            //If we are a client then force the protocol type to be written to the stream.
            if (this is TethysClient)
            {
                rawOutput.Write(EnumProtocol.TethysFrame.ToString());
                rawOutput.Write(Guid.Empty.ToString());
                rawOutput.Flush();
            }

            rawOutput.Write(ProtocolDescriptorBlock.ToString());
            rawOutput.Flush();

            var inputBlockString = rawInput.ReadString();
            var inputBlock = TethysProtocolDescriptorBlock.FromString(inputBlockString);

            //TODO: actually use the descriptor block for something

            rawInput.Dispose();
            rawOutput.Dispose();

            _inputStream = new SimpleStream(theStream);
            _outputStream = new SimpleStream(theStream);

            StartReadThread();
        }

        #region Certificate Callbacks

        private X509Certificate CertificateSelectionCallback(object sender, string targetHost, X509CertificateCollection localCertificates, X509Certificate remoteCertificate, string[] acceptableIssuers)
        {
            return SslConnectionProperties.Certificate;
        }

        private bool CertificateValidationCallback(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            if (sslPolicyErrors == SslPolicyErrors.None)
            {
                return true;
            }

            if (sslPolicyErrors == SslPolicyErrors.RemoteCertificateNotAvailable && this is TethysConnectedClient)
            {
                return !SslConnectionProperties.EnforceMutualAuthentication;
            }

            if (SslConnectionProperties.AllowSelfSigned && chain.ChainStatus.Length == 1 && sslPolicyErrors == SslPolicyErrors.RemoteCertificateChainErrors && chain.ChainStatus[0].Status == X509ChainStatusFlags.UntrustedRoot)
            {
                if (SslConnectionProperties.Certificate != null)
                {
                    return certificate.Equals(SslConnectionProperties.Certificate);
                }
                return true;
            }
            return false;
        }

        #endregion Certificate Callbacks

        public void Disconnect(string reason = null)
        {
            WritePacket(new TethysDisconnectPacket
            {
                Reason = reason
            });
            Dispose();
        }

        #region Thread Control

        public void StartReadThread()
        {
            if (_readThread != null)
                return;
            _die = false;
            _readThread = new Thread(ReadSocketData)
            {
                IsBackground = true,
                Name = GetType().Name + "-READER"
            };
            _readThread.Start();
        }

        public void KillReadThread()
        {
            _die = true;
            _readThread.Interrupt();
            if (_readThread.IsAlive) _readThread.Join();
            _die = false;
            _readThread = null;
        }

        #endregion Thread Control

        #region Packet Reading

        /// <summary>
        /// Thread function to read data from the socket.
        /// </summary>
        private void ReadSocketData()
        {
            while (!_die)
            {
                ReadOne();
            }
        }

        private bool ValidateMagic()
        {
            /*
             * Explanation for this bit because it's important:
             * - A "magic safety buffer" is prepended to each frame when it is sent.
             * - When the read thread starts the next cycle it will block on this ReadBytes.
             * - If the underlying stream times out, the information is "converted" into whatever is blocking read.
             * - If it times out on packetSize, then a packet of ridiculous size is expected.
             * - This would then feed into "read bytes".
             * - Read bytes allocates an array of several gigabytes before failing to read.
             * - This array is not cleaned up.
             * - The program crashes.
             *
             * If we make the stream crash on packetMagic, then packetSize read will fire the timeout.
             */
            var packetMagic = _inputStream.ReadBytes(6);

            Debug.WriteLine($"[{GetType().Name}] MAGIC: {Encoding.UTF8.GetString(packetMagic)}");
            var badMagic = false;
            for (var i = 0; i < 6; i++)
            {
                if (Magic[i] == packetMagic[i]) continue;
                badMagic = true;
                break;
            }

            if (!badMagic) return true;
            Debug.WriteLine($"[{Thread.CurrentThread.Name}] INVALID MAGIC");
            return false;
        }

        /// <summary>
        /// Read one packet from the input stream.
        /// </summary>
        private void ReadOne()
        {
            try
            {
                if (!ValidateMagic())
                    return;

                //Read the expected packet size TODO: could this be spoofed maliciously? Set max packet size? Only alloc as we read? Privileged clients via SSL identity?
                var packetSize = _inputStream.ReadInt();
                var packetBytes = _inputStream.ReadBytes(packetSize);
                var packet = PacketEncoder.DeserialiseObject<TethysPacket>(packetBytes);

                //If this packet has a response ID, check if we are currently awaiting a latch with the response ID it provides.
                if (packet.ResponseId.HasValue)
                {
                    lock (_responseLatches)
                    {
                        if (_responseLatches.ContainsKey(packet.ResponseId.Value))
                        {
                            _responseLatches[packet.ResponseId.Value].SetResult(packet);
                            return;
                        }
                    }
                }

                //If it isn't a response packet, fire the read event.
                packet.Source = this;
                ReadEvent?.Invoke(this, packet);
            }
            catch (ThreadInterruptedException)
            {
                //If the thread was interrupted, just let the while loop continue and check for _die.
                Debug.WriteLine("THREAD WAS INTERRUPTED.");
            }
            catch (IOException ex)
            {
                if (ex.InnerException is SocketException sex)
                {
                    if (sex.SocketErrorCode != SocketError.TimedOut)
                    {
                        IoFailure?.Invoke(this, ex);
                        throw;
                    }
                }
                else
                {
                    IoFailure?.Invoke(this, ex);
                    throw;
                }

                Debug.WriteLine(ex);
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
            }
        }

        #endregion Packet Reading

        #region Packet Writing

        /// <summary>
        /// Write a packet to the connection.
        /// </summary>
        /// <param name="packet">The packet to write</param>
        public void WritePacket(TethysPacket packet)
        {
            if (!packet.PacketId.HasValue)
                packet.PacketId = Guid.NewGuid();

            try
            {
                lock (_writeLockHandle)
                {
                    var bytes = PacketEncoder.SerialiseObject(packet);
                    //TODO: move the header construction elsewhere
                    _outputStream.Write(Magic); //safety buffer in case it breaks
                    _outputStream.Write(bytes.Length);
                    _outputStream.Write(bytes);
                    _outputStream.Flush();
                }
            }
            catch (IOException ex)
            {
                IoFailure?.Invoke(this, ex);
                throw;
            }
        }

        /// <summary>
        /// Write a packet to the connection asynchronously.
        /// </summary>
        /// <param name="packet">The packet to write</param>
        /// <returns>A task that will complete when the packet has been dispatched</returns>
        public Task WritePacketAsync(TethysPacket packet)
        {
            return Task.CompletedTask.ContinueWith(t => { WritePacket(packet); });
        }

        /// <summary>
        /// Write a packet to the connection and await a response from the other side. Warning: if incorrectly configured can cause deadlock.
        /// </summary>
        /// <typeparam name="TPacket">the type of packet expected as a response</typeparam>
        /// <param name="packet">The packet to write</param>
        /// <param name="enableTimeout">decide if this connection will be timed out if the other side takes too long</param>
        /// <param name="timeout">The timespan to wait before the attempt is called off</param>
        /// <returns></returns>
        public Task<TPacket> WriteAndAwaitResponseAsync<TPacket>(TethysPacket packet, bool enableTimeout = true, TimeSpan? timeout = null) where TPacket : TethysPacket
        {
            if (!packet.PacketId.HasValue)
                packet.PacketId = Guid.NewGuid();

            var ret = Task.CompletedTask.ContinueWith(t =>
            {
                //Prep the latch
                var tcs = new TaskCompletionSource<TethysPacket>();
                lock (_responseLatches)
                {
                    _responseLatches.Add(packet.PacketId.Value, tcs);
                }

                //Write the packet as normal
                WritePacket(packet);

                if (enableTimeout && timeout == null)
                    timeout = TimeSpan.FromSeconds(90);

                try
                {
                    //Wait on the latch to get the packet.
                    var tasks = timeout == null ? new[] { tcs.Task } : new[] { tcs.Task, Task.Delay(timeout.Value).ContinueWith(_ => (TethysPacket)null) };
                    var responseTasks = Task.WhenAny(tasks);
                    var response = responseTasks.GetAwaiter().GetResult().GetAwaiter().GetResult();
                    return response as TPacket;
                }
                finally
                {
                    //Remove the latch from the waiting queue.
                    lock (_responseLatches)
                    {
                        _responseLatches.Remove(packet.PacketId.Value);
                    }
                }
            });
            return ret;
        }

        #endregion Packet Writing

        /// <inheritdoc />
        /// <summary>
        /// Dispose of the connection.
        /// </summary>
        public void Dispose()
        {
            _die = true;
            _readThread?.Interrupt();
            _inputStream.Dispose();
            _outputStream.Dispose();

            //Unlock all the latches and allow them to fail
            foreach (var kvp in _responseLatches)
            {
                kvp.Value.SetCanceled();
            }
            _responseLatches.Clear();

            SocketAdapter.Dispose();
        }
    }
}