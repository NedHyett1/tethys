﻿using System;
using System.Threading;

namespace Tethys.Services
{
    public static class HeartbeatService
    {
        public static event EventHandler OnFirePing;

        private static Thread _pingThread;

        public static bool IsStarted()
        {
            return _pingThread != null;
        }

        public static void Start()
        {
            if (IsStarted())
                return;
            _pingThread = new Thread(PingLoop) { IsBackground = true };
            _pingThread.Start();
        }

        private static void PingLoop()
        {
            while (true)
            {
                try
                {
                    OnFirePing?.Invoke(null, null);
                }
                catch
                {
                }
                finally
                {
                    Thread.Sleep(TimeSpan.FromSeconds(30));
                }
            }
        }
    }
}