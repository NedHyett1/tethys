﻿using System.Reflection;

namespace Tethys.Header
{
    /// <summary>
    /// The protocol header base class.
    /// </summary>
    public abstract class TethysProtocolHeader
    {
        private readonly string _headerValue;

        protected TethysProtocolHeader(object rawValue)
        {
            _headerValue = Encode(rawValue);
        }

        internal TethysProtocolHeader(string headerValue)
        {
            _headerValue = headerValue;
        }

        public abstract object GetValue();

        protected abstract string Encode(object input);

        public ProtocolHeaderMetadataAttribute GetMetadata()
        {
            return GetType().GetCustomAttribute<ProtocolHeaderMetadataAttribute>();
        }

        public override string ToString()
        {
            return GetMetadata().Name + ": " + _headerValue;
        }
    }
}