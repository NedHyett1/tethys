﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Tethys.Header
{
    /// <summary>
    /// Used to store references to all <see cref="TethysProtocolHeader"/> classes in order to pick them out for deserialisation.
    /// </summary>
    public static class FieldRegistry
    {
        private static readonly Dictionary<string, Type> Headers = new Dictionary<string, Type>();

        /// <summary>
        /// Loads all classes if not already loaded.
        /// </summary>
        private static void Load()
        {
            if (Headers.Count > 0)
                return;

            var types = typeof(FieldRegistry).Assembly.GetTypes().Where(t => typeof(TethysProtocolHeader).IsAssignableFrom(t) && !t.IsAbstract);
            foreach (var type in types)
            {
                var meta = type.GetCustomAttribute<ProtocolHeaderMetadataAttribute>();
                Headers.Add(meta.Name, type);
            }
        }

        /// <summary>
        /// Gets a header by the metadata form attribute.
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static TethysProtocolHeader GetHeader(string id, string value)
        {
            Load();
            if (!Headers.ContainsKey(id))
                return null;
            var headerType = Headers[id];
            var constructor = headerType.GetConstructor(new[] { typeof(string) });
            return constructor.Invoke(new object[] { value }) as TethysProtocolHeader;
        }
    }
}