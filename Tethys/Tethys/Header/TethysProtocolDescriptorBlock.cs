﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tethys.Header
{
    /// <summary>
    /// Sent between client/server during connection initialisation to notify the remote about the
    /// configuration of the local.
    /// </summary>
    public class TethysProtocolDescriptorBlock
    {
        private List<TethysProtocolHeader> _headers = new List<TethysProtocolHeader>();

        /// <summary>
        /// Add a new header to the block.
        /// </summary>
        /// <param name="header"></param>
        public void AddHeader(TethysProtocolHeader header)
        {
            _headers.Add(header);
        }

        /// <summary>
        /// Get the header of the specified type from the block.
        /// </summary>
        /// <typeparam name="THeaderType"></typeparam>
        /// <returns></returns>
        public THeaderType GetHeader<THeaderType>() where THeaderType : TethysProtocolHeader
        {
            return (THeaderType)_headers.FirstOrDefault(hd => hd is THeaderType);
        }

        /// <summary>
        /// Create a block from a blockstring.
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static TethysProtocolDescriptorBlock FromString(string input)
        {
            var lines = input.Split("\n").Where(a => a.IndexOf(": ", StringComparison.Ordinal) > 0).Select(a =>
            {
                var idx = a.IndexOf(": ");
                var one = a.Substring(0, idx);
                var two = a.Substring(idx + ": ".Length);
                return new KeyValuePair<string, string>(one, two);
            }).Select(kvp => FieldRegistry.GetHeader(kvp.Key, kvp.Value)).ToList();
            return new TethysProtocolDescriptorBlock
            {
                _headers = lines
            };
        }

        /// <summary>
        /// Convert the block into a blockstring for transmission.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            foreach (var tethysProtocolHeader in _headers)
            {
                sb.AppendLine(tethysProtocolHeader.ToString());
            }

            return sb.ToString();
        }
    }
}