﻿using System;

namespace Tethys.Header
{
    /// <summary>
    /// Metadata pertaining to a protocol descriptor block field.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class)]
    public class ProtocolHeaderMetadataAttribute : Attribute
    {
        public string Name { get; }

        public ProtocolHeaderMetadataAttribute(string name)
        {
            Name = name;
        }
    }
}