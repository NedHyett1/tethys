﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Newtonsoft.Json;

using Tethys.Sockets.SocketAdapters;
using Tethys.Sockets.Udp;
using Tethys.Utility;

namespace Tethys.Sockets.Listener
{
    /// <summary>
    /// Provided to the server to listen for new UDP connections. Spawns <see cref="UdpSocketAdapter"/> instances.
    /// </summary>
    public class UdpListenerAdapter : ITethysListenerAdapter
    {
        private readonly UdpClient _udpClient;

        private Thread _readerThread;

        private readonly SynchronisedExchangeLock<(Guid, UdpClient)> _exchangeLock = new SynchronisedExchangeLock<(Guid, UdpClient)>();

        internal event EventHandler<(Guid, byte[])> OnUserFrame;

        public UdpListenerAdapter(int port) : this(new UdpClient(port))
        {
        }

        public UdpListenerAdapter(UdpClient udpClient)
        {
            _udpClient = udpClient;
            _readerThread = new Thread(Read) { IsBackground = true };
            _readerThread.Start();
        }

        private void Read()
        {
            while (true)
            {
                //try
                {
                    IPEndPoint ipep = null;
                    var buf = _udpClient.Receive(ref ipep);
                    var ms = new MemoryStream(buf);
                    var frame = UdpFrame.ReadFrom(ms);
                    if (frame.IsSystemMessage || frame.ClientId == Guid.Empty)
                    {
                        HandleSystemMessage(frame, ipep);
                    }
                    else
                    {
                        //Invoke the event with the client id
                        OnUserFrame?.Invoke(this, (frame.ClientId, frame.Payload));
                    }
                }
                //catch (Exception e)
                {
                    //throw;
                }
            }
        }

        /// <summary>
        /// Handle a system message
        /// </summary>
        /// <param name="frame"></param>
        /// <param name="ipep"></param>
        private void HandleSystemMessage(UdpFrame frame, IPEndPoint ipep)
        {
            var payloadStr = Encoding.UTF8.GetString(frame.Payload);
            var payload = JsonConvert.DeserializeObject<Dictionary<string, object>>(payloadStr);
            switch (payload["PACKET_TYPE"] as string)
            {
                case "ID_REQ":
                    {
                        var newClientId = Guid.NewGuid();
                        var newUdpClient = new UdpClient(); //Target the new udp client at the endpoint
                        newUdpClient.Connect(ipep);
                        NotifyClientOfGuid(newUdpClient, newClientId);
                        _exchangeLock.AwaitReset();
                        _exchangeLock.Give((newClientId, newUdpClient));
                    }
                    break;
            }
        }

        private void NotifyClientOfGuid(UdpClient c, Guid g)
        {
            var dict = new Dictionary<string, object>()
            {
                {"PACKET_TYPE", "ID_ASSIGN"},
                {"NEW_ID", g.ToString()}
            };
            var dictStr = JsonConvert.SerializeObject(dict);
            var dictStrBytes = Encoding.UTF8.GetBytes(dictStr);
            using (var ms = new MemoryStream())
            {
                var frame = new UdpFrame()
                {
                    ClientId = Guid.Empty,
                    Flow = 0,
                    IsSystemMessage = true,
                    Timestamp = 0,
                    Payload = dictStrBytes
                };
                frame.WriteTo(ms);
                var payload = ms.ToArray();
                c.Send(payload, payload.Length);
                Debug.WriteLine($"SENT ID {g}");
            }
        }

        public ITethysSocketAdapter AcceptClient()
        {
            var (item1, udpClient) = _exchangeLock.Get();
            var usa = new UdpSocketAdapter(this, item1, udpClient);
            ((UdpSocketAdapterStream)usa.GetStream()).UdpNegotiationsComplete();
            return usa;
        }
    }
}