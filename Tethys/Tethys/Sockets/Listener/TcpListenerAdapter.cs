﻿using System.Net;
using System.Net.Sockets;

using Tethys.Sockets.SocketAdapters;

namespace Tethys.Sockets.Listener
{
    public class TcpListenerAdapter : ITethysListenerAdapter
    {
        private readonly TcpListener _listener;
        private readonly IPAddress _listeningAddress;
        private readonly int _port;

        public TcpListenerAdapter(TcpListener listener)
        {
            _listener = listener;
            _listeningAddress = ((IPEndPoint)_listener.LocalEndpoint).Address;
            _port = ((IPEndPoint)_listener.LocalEndpoint).Port;
            _listener.Start();
        }

        public TcpListenerAdapter(int port, IPAddress listeningAddress = null) : this(new TcpListener(listeningAddress ?? IPAddress.Any, port))
        {
        }

        public ITethysSocketAdapter AcceptClient()
        {
            return new TcpSocketAdapter(_listener.AcceptTcpClient());
        }
    }
}