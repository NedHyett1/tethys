﻿using System;
using System.IO;
using System.Net;

namespace Tethys.Sockets
{
    public interface ITethysSocketAdapter : IDisposable
    {
        bool IsConnected();

        IPEndPoint GetRemoteAddress();

        void SetSendTimeout(int timeout);

        int GetSendTimeout();

        void SetReceiveTimeout(int timeout);

        int GetReceiveTimeout();

        Stream GetStream();
    }
}