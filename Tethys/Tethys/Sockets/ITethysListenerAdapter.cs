﻿namespace Tethys.Sockets
{
    public interface ITethysListenerAdapter
    {
        ITethysSocketAdapter AcceptClient();
    }
}