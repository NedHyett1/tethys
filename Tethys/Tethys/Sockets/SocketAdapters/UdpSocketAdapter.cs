﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

using Tethys.Services;
using Tethys.Sockets.Listener;
using Tethys.Sockets.Udp;
using Tethys.Utility;

namespace Tethys.Sockets.SocketAdapters
{
    /// <summary>
    /// Created by the <see cref="UdpListenerAdapter"/> to hold data about a "connected" client over UDP. Transparent to the rest
    /// of the engine, but holds data for maintaining the conversation between the client and server.
    /// </summary>
    public class UdpSocketAdapter : ITethysSocketAdapter, IUdpFeeder
    {
        private readonly UdpListenerAdapter _listener;
        private readonly Guid _clientId;
        private readonly UdpClient _udpClient;
        private readonly UdpSocketAdapterStream _managedStream;

        private readonly ConcurrentQueue<byte[]> _readQueue = new ConcurrentQueue<byte[]>();

        private bool _heartbeatKillswitch = true;

        public UdpSocketAdapter(UdpListenerAdapter listener, Guid clientId, UdpClient udpClient)
        {
            _udpClient = udpClient;
            _listener = listener;
            _clientId = clientId;
            listener.OnUserFrame += Listener_OnUserFrame;
            _managedStream = new UdpSocketAdapterStream(_udpClient, this);
            //HeartbeatService.Start();
            HeartbeatService.OnFirePing += HeartbeatService_OnFirePing;
        }

        private void HeartbeatService_OnFirePing(object sender, EventArgs e)
        {
            if (!_heartbeatKillswitch)
            {
                Disconnect();
                Dispose();
                return;
            }

            _heartbeatKillswitch = false;

            var dict = new Dictionary<string, object>()
            {
                {"PACKET_TYPE", "HEARTBEAT"},
                {"HEARTBEAT_KEY", Guid.NewGuid()}
            }.ToJsonBytes();
            _managedStream.WriteSystemMessage(dict);
        }

        private void Listener_OnUserFrame(object sender, (Guid clientId, byte[] payload) e)
        {
            if (e.clientId == _clientId)
                _readQueue.Enqueue(e.payload);
        }

        public void Dispose()
        {
            _udpClient?.Dispose();
            _managedStream?.Dispose();
        }

        public void Connect()
        {
        }

        public void Disconnect()
        {
        }

        public bool IsConnected()
        {
            if (_udpClient == null)
                return false;
            return _udpClient.Client.Connected;
        }

        public IPEndPoint GetRemoteAddress()
        {
            return (IPEndPoint)_udpClient.Client.RemoteEndPoint;
        }

        public void SetSendTimeout(int timeout)
        {
            //NOOP
        }

        public int GetSendTimeout()
        {
            return 0;
        }

        public void SetReceiveTimeout(int timeout)
        {
            //NOOP
        }

        public int GetReceiveTimeout()
        {
            return 0;
        }

        public Stream GetStream()
        {
            return _managedStream;
        }

        public byte[] RequestData()
        {
            while (true)
            {
                if (_readQueue.TryDequeue(out var result))
                    return result;
                else
                    Thread.Sleep(50);
            }
        }

        public Guid GetClientId()
        {
            return _clientId;
        }

        ~UdpSocketAdapter()
        {
            HeartbeatService.OnFirePing -= HeartbeatService_OnFirePing;
            _listener.OnUserFrame -= Listener_OnUserFrame;
        }
    }
}