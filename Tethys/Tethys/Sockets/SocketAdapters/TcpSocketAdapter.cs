﻿using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Tethys.Sockets.SocketAdapters
{
    /// <summary>
    /// Sits between Tethys and the TCP Protocol to allow for easy implementation of additional protocols.
    /// </summary>
    public class TcpSocketAdapter : ITethysSocketAdapter
    {
        private readonly TcpClient _tcpClient;

        /// <summary>
        /// Create an adapter for an already existing <see cref="TcpClient"/>.
        /// </summary>
        /// <param name="tcpClient"></param>
        public TcpSocketAdapter(TcpClient tcpClient)
        {
            _tcpClient = tcpClient;
            _tcpClient.NoDelay = true;
        }

        /// <summary>
        /// Create an adapter for a hostname and port without connecting.
        /// </summary>
        /// <param name="hostname"></param>
        /// <param name="port"></param>
        public TcpSocketAdapter(string hostname, int port) : this(new TcpClient(hostname, port))
        {
        }

        /// <summary>
        /// Create an adapter for an <see cref="IPEndPoint"/> without connecting.
        /// </summary>
        /// <param name="endPoint"></param>
        public TcpSocketAdapter(IPEndPoint endPoint) : this(endPoint.Address.ToString(), endPoint.Port)
        {
        }

        public bool IsConnected()
        {
            if (_tcpClient == null)
                return false;
            return _tcpClient.Connected || _tcpClient.Client.Connected;
        }

        public IPEndPoint GetRemoteAddress()
        {
            return (IPEndPoint)_tcpClient.Client.RemoteEndPoint;
        }

        public void SetSendTimeout(int timeout)
        {
            _tcpClient.SendTimeout = timeout;
        }

        public int GetSendTimeout()
        {
            return _tcpClient.SendTimeout;
        }

        public void SetReceiveTimeout(int timeout)
        {
            _tcpClient.ReceiveTimeout = timeout;
        }

        public int GetReceiveTimeout()
        {
            return _tcpClient.ReceiveTimeout;
        }

        public Stream GetStream()
        {
            return _tcpClient.GetStream();
        }

        public void Dispose()
        {
            _tcpClient?.Close();
            _tcpClient?.Dispose();
        }
    }
}