﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

using Newtonsoft.Json;

using Tethys.Sockets.Udp;
using Tethys.Utility;

namespace Tethys.Sockets.SocketAdapters
{
    /// <summary>
    /// Given to the client to handle formatting the UDP packets as a stream transparently.
    /// </summary>
    public class UdpClientSocketAdapter : ITethysSocketAdapter, IUdpFeeder
    {
        private readonly UdpClient _udpClient;
        private readonly UdpSocketAdapterStream _stream;

        private Guid _clientId = Guid.Empty;

        public UdpClientSocketAdapter(string host, int port)
        {
            _udpClient = new UdpClient();
            _udpClient.Connect(host, port);
            _stream = new UdpSocketAdapterStream(_udpClient, this);
            var dict = new Dictionary<string, object>()
            {
                {"PACKET_TYPE", "ID_REQ"}
            }.ToJsonBytes();
            _stream.WriteSystemMessage(dict);
            new Thread(ForceReadCycles) { IsBackground = true }.Start();
        }

        private void ForceReadCycles()
        {
            while (_clientId == Guid.Empty)
            {
                Debug.WriteLine("FUG");
                RequestData();
            }
        }

        public UdpClientSocketAdapter(UdpClient udpClient)
        {
            _udpClient = udpClient;
            _stream = new UdpSocketAdapterStream(udpClient, this);
            var dict = new Dictionary<string, object>()
            {
                {"PACKET_TYPE", "ID_REQ"}
            }.ToJsonBytes();
            _stream.WriteSystemMessage(dict);
        }

        public byte[] RequestData()
        {
            while (true)
            {
                try
                {
                    IPEndPoint ipep = null;
                    var buf = _udpClient.Receive(ref ipep);
                    var ms = new MemoryStream(buf);
                    var frame = UdpFrame.ReadFrom(ms);
                    if (frame.IsSystemMessage)
                    {
                        HandleSystemMessage(frame);
                    }
                    else
                    {
                        return frame.Payload;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw e;
                }
            }
        }

        public Guid GetClientId()
        {
            return _clientId;
        }

        private void HandleSystemMessage(UdpFrame frame)
        {
            var payloadStr = Encoding.UTF8.GetString(frame.Payload);
            var payload = JsonConvert.DeserializeObject<Dictionary<string, object>>(payloadStr);
            switch (payload["PACKET_TYPE"] as string)
            {
                case "ID_ASSIGN":
                    _clientId = Guid.Parse(payload["NEW_ID"] as string);
                    _stream.UdpNegotiationsComplete();
                    break;

                case "HEARTBEAT":
                    SendHeartbeat(payload["HEARTBEAT_KEY"] as string);
                    break;

                case "DISCONNECT":
                    //TODO: implement
                    break;

                default:
                    throw new InvalidOperationException($"Invalid System Message {payload["PACKET_TYPE"]}");
            }
        }

        private void SendHeartbeat(string heartbeatKey)
        {
        }

        public void Dispose()
        {
            Disconnect();
        }

        public void Connect()
        {
        }

        public void Disconnect()
        {
            _udpClient.Dispose();
        }

        public bool IsConnected()
        {
            return true;
        }

        public IPEndPoint GetRemoteAddress()
        {
            return ((IPEndPoint)_udpClient.Client.RemoteEndPoint);
        }

        public void SetSendTimeout(int timeout)
        {
        }

        public int GetSendTimeout()
        {
            return 0;
        }

        public void SetReceiveTimeout(int timeout)
        {
        }

        public int GetReceiveTimeout()
        {
            return 0;
        }

        public Stream GetStream()
        {
            return _stream;
        }
    }
}