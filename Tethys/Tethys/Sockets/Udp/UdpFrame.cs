﻿using System;
using System.IO;

using Tethys.Utility.Streams;

namespace Tethys.Sockets.Udp
{
    public sealed class UdpFrame
    {
        public static UdpFrame ReadFrom(byte[] buf)
        {
            using (var ms = new MemoryStream(buf))
                return ReadFrom(ms);
        }

        public static UdpFrame ReadFrom(Stream stream)
        {
            var ss = new SimpleStream(stream, true);
            var isSystemMessage = ss.ReadBool();
            var clientId = Guid.Parse(ss.ReadString());
            var flow = ss.ReadLong();
            var ts = ss.ReadLong();
            var payload = ss.ReadTrackedBytes();
            return new UdpFrame
            {
                IsSystemMessage = isSystemMessage,
                ClientId = clientId,
                Flow = flow,
                Timestamp = ts,
                Payload = payload
            };
        }

        public bool IsSystemMessage;
        public Guid ClientId;
        public long Flow;
        public long Timestamp;
        public byte[] Payload;

        public byte[] AsBytes()
        {
            using (var ms = new MemoryStream())
            {
                WriteTo(ms);
                return ms.ToArray();
            }
        }

        public void WriteTo(Stream stream)
        {
            var ss = new SimpleStream(stream, true);
            ss.Write(IsSystemMessage);
            ss.Write(ClientId.ToString());
            ss.Write(Flow);
            ss.Write(Timestamp);
            ss.WriteTrackedBytes(Payload);
            ss.Dispose();
        }
    }
}