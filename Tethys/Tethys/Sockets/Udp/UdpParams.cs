﻿using System;

namespace Tethys.Sockets.Udp
{
    public static class UdpParams
    {
        private static Guid _clientId = Guid.Empty;

        public static Guid ClientId
        {
            get
            {
                if (_clientId == Guid.Empty)
                    _clientId = Guid.NewGuid();
                return _clientId;
            }
            set => _clientId = value;
        }
    }
}