﻿using System;

namespace Tethys.Sockets.Udp
{
    /// <summary>
    /// Implemented by UDP feeder adapters that handle request data. Blocks until a userland frame
    /// arrives for processing, allowing the feed request loop to be driven by the user thread instead
    /// of needlessly spawning a thread to process internal data.
    ///
    /// (i.e. user thread calls read on stream -> stream requests data from the feeder ->
    /// feeder waits until data is available and/or processes system frames -> when userland frame arrives,
    /// it is pushed through the feeder -> feeder feeds the bytes to the stream -> stream reads the bytes back
    /// transparently to the user as if they were streamed in)
    /// </summary>
    public interface IUdpFeeder
    {
        /// <summary>
        /// Request data from the feeder.
        /// </summary>
        /// <returns></returns>
        byte[] RequestData();

        Guid GetClientId();
    }
}