﻿using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Threading;

namespace Tethys.Sockets.Udp
{
    /// <summary>
    /// Impersonates a standard network stream interface in order to fool the rest of Tethys into believing that it is
    /// a contiguous stream. Must flush to send data.
    /// </summary>
    internal class UdpSocketAdapterStream : Stream
    {
        private readonly UdpClient _udpClient;
        private readonly IUdpFeeder _feeder;

        private readonly object _writeLock = new object();
        private readonly object _readLock = new object();

        private readonly MemoryStream _writeBuffer = new MemoryStream();
        private MemoryStream _readBuffer = new MemoryStream();

        private CountdownEvent _cde = new CountdownEvent(1);

        public UdpSocketAdapterStream(UdpClient udpClient, IUdpFeeder feeder)
        {
            _udpClient = udpClient;
            _feeder = feeder;
        }

        internal void UdpNegotiationsComplete()
        {
            Debug.WriteLine("FUK");
            _cde.Signal();
        }

        private void WriteMessage(byte[] b, bool isSystem = false)
        {
            var frame = new UdpFrame()
            {
                ClientId = _feeder.GetClientId(),
                Flow = 0,
                IsSystemMessage = isSystem,
                Timestamp = 0,
                Payload = b
            };

            using (var ms = new MemoryStream())
            {
                frame.WriteTo(ms);

                var fullPayload = ms.ToArray();

                _udpClient.Send(fullPayload, fullPayload.Length);
            }
        }

        internal void WriteSystemMessage(byte[] b)
        {
            lock (_writeLock)
            {
                WriteMessage(b, true);
            }
        }

        public override void Flush()
        {
            _cde.Wait();
            lock (_writeLock)
            {
                var b = _writeBuffer.ToArray();
                _writeBuffer.SetLength(0); //clear it

                WriteMessage(b);
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            lock (_readLock)
            {
                //Keep trying to read data out of the feeder until we have enough to satisfy the count
                while (_readBuffer.Length - _readBuffer.Position < count)
                {
                    var posStore = _readBuffer.Position;
                    _readBuffer.Position = _readBuffer.Length;
                    _readBuffer.Write(_feeder.RequestData());
                    _readBuffer.Position = posStore;
                }
                var ret = _readBuffer.Read(buffer, offset, count);
                var data = _readBuffer.ToArray().Skip((int)_readBuffer.Position).ToArray();
                _readBuffer = new MemoryStream(data);
                return ret;
            }
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            return 0;
        }

        public override void SetLength(long value)
        {
            //NOOP
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            lock (_writeLock)
                _writeBuffer.Write(buffer, offset, count);
        }

        public override bool CanRead => true;
        public override bool CanSeek => false;
        public override bool CanWrite => true;
        public override long Length => 0;

        public override long Position
        {
            get => 0;
            set { }
        }
    }
}