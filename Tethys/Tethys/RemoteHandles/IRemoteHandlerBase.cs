﻿namespace Tethys.RemoteHandles
{
    /// <summary>
    /// Dummy interface to group handlers together.
    /// </summary>
    public interface IRemoteHandlerBase
    {
    }
}