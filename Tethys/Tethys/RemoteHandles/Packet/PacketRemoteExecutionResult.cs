﻿using System;

using Tethys.Client;
using Tethys.Packet;
using Tethys.Server;

namespace Tethys.RemoteHandles.Packet
{
    [Serializable]
    public class PacketRemoteExecutionResult : TethysPacket
    {
        public object Result { get; set; }

        public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
        {
            throw new NotImplementedException();
        }

        public override void ProcessPacketClient(TethysClient client)
        {
            throw new NotImplementedException();
        }
    }
}