﻿using System;
using System.Collections.Generic;

using Tethys.Client;
using Tethys.Packet;
using Tethys.Server;

namespace Tethys.RemoteHandles.Packet
{
    [Serializable]
    public class PacketPerformRemoteExecution : TethysPacket
    {
        public string ClassName { get; set; }
        public string Function { get; set; }
        public Dictionary<string, object> Parameters { get; set; }

        public PacketPerformRemoteExecution()
        {
        }

        public PacketPerformRemoteExecution(string className, string function, Dictionary<string, object> parameters)
        {
            ClassName = className;
            Function = function;
            Parameters = parameters;
        }

        public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
        {
            var response = client.WriteAndAwaitResponseAsync<PacketRemoteExecutionResult>(this).GetAwaiter().GetResult();
            Reply(response);
        }

        public override void ProcessPacketClient(TethysClient client)
        {
            var result = RemoteApi.HandleInbound(ClassName, Function, Parameters);
            Reply(new PacketRemoteExecutionResult
            {
                Result = result
            });
        }
    }
}