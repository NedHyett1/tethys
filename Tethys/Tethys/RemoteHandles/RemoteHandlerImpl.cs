﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Tethys.RemoteHandles.Packet;

namespace Tethys.RemoteHandles
{
    public class RemoteHandlerImpl
    {
        public static Task<TResult> TransmitToIpc<TResult>(Dictionary<string, object> parameters, string methodName, string className)
        {
            return Task.CompletedTask.ContinueWith(t =>
            {
                var packet = new PacketPerformRemoteExecution
                {
                    ClassName = className,
                    Function = methodName,
                    Parameters = parameters
                };
                Console.WriteLine($"CREATING PACKET FOR METHOD: {packet.Function}");
                Console.WriteLine("With the following parameters:");
                foreach (var param in packet.Parameters)
                    Console.WriteLine($@"Parameter: '{param.Key}' | Value: '{param.Value}'");

                var response = RemoteApi.Connection.WriteAndAwaitResponseAsync<PacketRemoteExecutionResult>(packet);
                return (TResult)response.GetAwaiter().GetResult().Result;
            });
        }
    }
}