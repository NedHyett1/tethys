﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tethys.RemoteHandles
{
    /// <summary>
    /// The configuration class for the RemoteApi.
    /// </summary>
    public static class RemoteApi
    {
        /// <summary>
        /// The connection upon which to run the API
        /// </summary>
        public static TethysConnection Connection { get; set; }

        /// <summary>
        /// The Remote Handler implementation to invoke calls upon
        /// </summary>
        public static List<IRemoteHandlerBase> Handlers { get; } = new List<IRemoteHandlerBase>();

        public static void AddHandler(IRemoteHandlerBase handler)
        {
            Handlers.Add(handler);
        }

        internal static object HandleInbound(string className, string methodName, Dictionary<string, object> parameters)
        {
            var handler = Handlers.FirstOrDefault(h => h.GetType().FullName == className);
            if (handler == null)
                return null;
            var handlerType = handler.GetType();

            var function = handlerType.GetMethod(methodName);
            var orderedParameters = function.GetParameters().Select(p => parameters[p.Name]).ToArray();

            var result = (Task)function.Invoke(handler, orderedParameters);
            result.ConfigureAwait(false);
            return (object)((dynamic)result).Result;
        }

        /// <summary>
        /// Get a handle to the remote object.
        /// </summary>
        /// <typeparam name="THandleType"></typeparam>
        /// <returns></returns>
        public static THandleType GetHandle<THandleType>() where THandleType : class, IRemoteHandlerBase
        {
            return Activator.CreateInstance(RemoteHandleTypeGenerator.Wrap(typeof(THandleType))) as THandleType;
        }
    }
}