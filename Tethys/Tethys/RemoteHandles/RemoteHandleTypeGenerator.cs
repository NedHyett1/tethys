﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading.Tasks;

namespace Tethys.RemoteHandles
{
    internal static class RemoteHandleTypeGenerator
    {
        private static Dictionary<Type, Type> _cache { get; } = new Dictionary<Type, Type>();

        public static Type Wrap(Type serviceInterface)
        {
            if (_cache.ContainsKey(serviceInterface))
                return _cache[serviceInterface];

            if (!serviceInterface.IsInterface)
                throw new Exception("Not interface");

            if (!typeof(IRemoteHandlerBase).IsAssignableFrom(serviceInterface))
                throw new Exception("Not IRemoteHandlerBase");

            var type = GenerateType(serviceInterface);

            var methods = serviceInterface.GetMethods().ToList();

            var otherMethods = serviceInterface.GetInterfaces().SelectMany(i => i.GetMethods());
            methods.AddRange(otherMethods);

            foreach (var mi in methods)
            {
                Debug.WriteLine($"IMPLEMENTING {mi.Name}");
                ImplementMethod(type, mi, serviceInterface);
            }

            var ret = type.CreateType();
            _cache.Add(serviceInterface, ret);
            return ret;
        }

        private static TypeBuilder GenerateType(Type serviceInterface)
        {
            var assemblyName = new AssemblyName($"Tethys_Temp_Assembly_{serviceInterface.FullName}");
            var modName = $"{assemblyName.Name}.dll";
            var ns = serviceInterface.Namespace;
            if (!string.IsNullOrEmpty(ns))
                ns += ".";

            var assembly = AssemblyBuilder.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
            var mod = assembly.DefineDynamicModule(modName);
            var type = mod.DefineType($"{ns}Tethys_Serv_{serviceInterface.Name}", TypeAttributes.Class | TypeAttributes.AnsiClass | TypeAttributes.Sealed | TypeAttributes.Public);
            type.AddInterfaceImplementation(serviceInterface);
            type.SetParent(typeof(RemoteHandlerImpl));
            return type;
        }

        private static void ImplementMethod(TypeBuilder tb, MethodInfo mi, Type handlerType)
        {
            if (!typeof(Task).IsAssignableFrom(mi.ReturnType))
                throw new Exception("Return Type MUST be task");

            var transmit = typeof(RemoteHandlerImpl).GetMethod("TransmitToIpc", new[] { typeof(Dictionary<string, object>), typeof(string) }) ?? throw new Exception("Can't get transmit method?!?!");
            var generics = mi.ReturnType.GetGenericArguments();
            transmit = transmit.MakeGenericMethod(generics.Length > 0 ? generics[0] : typeof(object)); // convert the transmit to the generic version matching the method we are implementing

            var args = mi.GetParameters();

            var methodImpl = tb.DefineMethod(mi.Name, MethodAttributes.Public | MethodAttributes.Virtual, mi.ReturnType,
                (from arg in args select arg.ParameterType).ToArray());
            tb.DefineMethodOverride(methodImpl, mi);

            /*
             * New Plan:
             *
             * - Get the method arguments as a dictionary
             * - Invoke transmit
             * - Transmit finds the method name
             * - Wait for return
             *
             * TODO: include support for generic and overloaded methods
             *
             */

            var generator = methodImpl.GetILGenerator();

            var dictType = typeof(Dictionary<string, object>);
            var dictAddMethod = dictType.GetMethod("Add") ?? throw new Exception("Can't find dictionary add?!?!?");

            var dictImpl = generator.DeclareLocal(dictType); //declare a local storage space for the dictionary if we need to pop it from the eval stack

            generator.Emit(OpCodes.Newobj, dictType.GetConstructor(Type.EmptyTypes) ?? throw new Exception("Can't find dictionary ctor?!?!")); // create new dictionary on eval stack
            generator.Emit(OpCodes.Stloc, dictImpl);

            for (var paramIndex = 0; paramIndex < args.Length; paramIndex++) //for each parameter of method
            {
                generator.Emit(OpCodes.Ldloc, dictImpl); //load the dict onto the eval stack
                generator.Emit(OpCodes.Ldstr, args[paramIndex].Name); //load the method parameter name as the dict key
                EmitLoadArg(generator, paramIndex); //load the method parameter value as the dict value
                generator.Emit(OpCodes.Callvirt, dictAddMethod); //using the eval stack as [dict] [key] [value], virtcall the add method on the dictionary object, everything is popped from the eval stack
            }

            generator.Emit(OpCodes.Ldloc, dictImpl); //load the dictionary onto the eval stack to pass as the parameter to the transmit call
            generator.Emit(OpCodes.Ldstr, mi.Name);
            generator.Emit(OpCodes.Ldstr, handlerType.FullName);

            //call transmit function and place result on the eval stack
            generator.Emit(OpCodes.Call, transmit);

            //return value
            generator.Emit(OpCodes.Ret);
        }

        private static void EmitLoadArg(ILGenerator gen, int idx)
        {
            switch (idx)
            {
                case 0:
                    gen.Emit(OpCodes.Ldarg_1);
                    break;

                case 1:
                    gen.Emit(OpCodes.Ldarg_2);
                    break;

                case 2:
                    gen.Emit(OpCodes.Ldarg_3);
                    break;

                default:
                    gen.Emit((idx < 255) ? OpCodes.Ldarg_S : OpCodes.Ldarg, idx + 1);
                    break;
            }
        }
    }
}