﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;

using NUnit.Framework;

using Tethys.Client;
using Tethys.Packet;
using Tethys.Server;

namespace Tests
{
    public class StandardModelTests
    {
        private TethysServerBase _server;
        private TethysClient _client;

        [SetUp]
        public void SetUp()
        {
        }

        [Test]
        public void ConnectToServer()
        {
            var clientId = Guid.NewGuid();
            _server = new TethysServer(6666);
            _client = new TethysClient("localhost", 6666, clientId);
            var tcs = new TaskCompletionSource<bool>();
            _server.OnClientConnected += (sender, connectedClient) =>
            {
                Assert.IsTrue(connectedClient.ConnectionId == clientId);
                tcs.SetResult(true);
            };
            var tcsTask = tcs.Task;
            Assert.IsTrue(Task.WhenAny(tcsTask, Task.Delay(TimeSpan.FromSeconds(30))).GetAwaiter().GetResult() == tcsTask, "connection timed out");
        }

        [Test]
        public void SendDataClientToServer()
        {
            var clientId = Guid.NewGuid();
            _server = new TethysServer(6969);
            _server.Start();
            var cTask = Task.CompletedTask.ContinueWith(t => { _client = new TethysClient("localhost", 6969, clientId); _client.Connect(); });
            var tcs = new TaskCompletionSource<bool>();
            _server.OnClientConnected += (sender, connectedClient) =>
            {
                Assert.IsTrue(connectedClient.ConnectionId == clientId);
                tcs.SetResult(true);
            };
            var tcsTask = tcs.Task;
            Assert.IsTrue(Task.WhenAny(tcsTask, Task.Delay(TimeSpan.FromSeconds(10))).GetAwaiter().GetResult() == tcsTask, "connection timed out");
            cTask.GetAwaiter().GetResult();
            var response = _client.WriteAndAwaitResponseAsync<TestPacket>(new TestPacket
            {
                TestData = 123
            }).GetAwaiter().GetResult();
            Assert.IsTrue(response.TestData == 124);
        }

        [Test]
        public void SendDataServerToClient()
        {
            var clientId = Guid.NewGuid();
            _server = new TethysServer(6667);
            _client = new TethysClient("localhost", 6667, clientId);
            var tcs = new TaskCompletionSource<bool>();
            _server.OnClientConnected += (sender, connectedClient) =>
            {
                Assert.IsTrue(connectedClient.ConnectionId == clientId);
                tcs.SetResult(true);
            };
            var tcsTask = tcs.Task;
            Assert.IsTrue(Task.WhenAny(tcsTask, Task.Delay(TimeSpan.FromSeconds(90))).GetAwaiter().GetResult() == tcsTask, "connection timed out");

            var serverClient = _server.GetClient(clientId);
            var response = serverClient.WriteAndAwaitResponseAsync<TestPacket>(new TestPacket
            {
                TestData = 123
            }).GetAwaiter().GetResult();
            Assert.IsTrue(response.TestData == 124);
        }

        [Test]
        public void ManyClients()
        {
            _server = new TethysServer(6668);
            _server.OnClientConnected += (sender, client) => { Debug.WriteLine($"Client {client.ConnectionId} connected"); };

            var clients = new List<TethysClient>();
            Parallel.For(0, 1_000_000, (x) =>
            {
                var client = new TethysClient("localhost", 6668);
                clients.Add(client);
            });

            Task.Delay(30).GetAwaiter().GetResult();

            Debug.Assert(_server.NumberOfClients == clients.Count);
        }
    }

    [Serializable]
    public class TestPacket : TethysPacket
    {
        public int TestData { get; set; }

        public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
        {
            Reply(new TestPacket
            {
                TestData = TestData + 1
            });
        }

        public override void ProcessPacketClient(TethysClient client)
        {
            Reply(new TestPacket
            {
                TestData = TestData + 1
            });
        }
    }
}