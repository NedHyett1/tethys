﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Tethys.BinaryAsideStream;
using Tethys.Client;
using Tethys.Server;

namespace Tests
{
    public class BinaryAsideStreamTests
    {

        private TethysServerBase _server;
        private TethysClient _client;

        [Test]
        public void SendDataToClientTest()
        {
            BinaryAsideStreamProtocolRegistry.RegisterProtocol("BasTest", new BasTestProtocol());
            var clientId = Guid.NewGuid();
            _server = new TethysServer(6667);
            _client = new TethysClient("localhost", 6667, clientId);
            var tcs = new TaskCompletionSource<bool>();
            _server.OnClientConnected += (sender, connectedClient) =>
            {
                Assert.IsTrue(connectedClient.ConnectionId == clientId);
                tcs.SetResult(true);
            };
            var tcsTask = tcs.Task;
            Assert.IsTrue(Task.WhenAny(tcsTask, Task.Delay(TimeSpan.FromSeconds(90))).GetAwaiter().GetResult() == tcsTask, "connection timed out");

            var stream = _client.RequestBinaryAsideStream("BasTest").GetAwaiter().GetResult();
            var reader = new BinaryReader(stream);
            var b = reader.ReadBytes(17);
            var b2 = new byte[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1};
            for (var i = 0; i < b.Length; i++)
            {
                Console.WriteLine($"b: {b[i]} == b2: {b2[i]}?");
                Assert.IsTrue(b[i] == b2[i]);
            }
            reader.Dispose();
            stream.Dispose();
        }

        [Test]
        public void SendDataToServerTest()
        {
            BinaryAsideStreamProtocolRegistry.RegisterProtocol("BasTest", new BasTestProtocol());
            var clientId = Guid.NewGuid();
            _server = new TethysServer(6667);
            _client = new TethysClient("localhost", 6667, clientId);
            var tcs = new TaskCompletionSource<bool>();
            _server.OnClientConnected += (sender, connectedClient) =>
            {
                Assert.IsTrue(connectedClient.ConnectionId == clientId);
                tcs.SetResult(true);
            };
            var tcsTask = tcs.Task;
            Assert.IsTrue(Task.WhenAny(tcsTask, Task.Delay(TimeSpan.FromSeconds(90))).GetAwaiter().GetResult() == tcsTask, "connection timed out");

            var stream = _server.GetClient(clientId).RequestBinaryAsideStream("BasTest").GetAwaiter().GetResult();
            var reader = new BinaryReader(stream);
            var b = reader.ReadBytes(17);
            var b2 = new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
            for (var i = 0; i < b.Length; i++)
            {
                Console.WriteLine($"b: {b[i]} == b2: {b2[i]}?");
                Assert.IsTrue(b[i] == b2[i]);
            }
            reader.Dispose();
            stream.Dispose();
        }

    }

    public class BasTestProtocol : IBinaryAsideStreamProtocol
    {
        public void HandleRemote(Stream stream)
        {
            using (var writer = new BinaryWriter(stream, Encoding.UTF8, true))
            {
                writer.Write(new byte[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 8, 7, 6, 5, 4, 3, 2, 1 });
                writer.Flush();
            }
        }

        public void HandleLocal(Stream stream)
        {
            throw new NotImplementedException();
        }
    }
}
