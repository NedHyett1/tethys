﻿using System;
using System.Threading.Tasks;

using NUnit.Framework;

using Tethys.Client;
using Tethys.Server;
using Tethys.Sockets.Listener;
using Tethys.Sockets.SocketAdapters;

namespace Tests
{
    public class UdpTests
    {
        private TethysServer _server;
        private TethysClient _client;

        [Test]
        public void Fug()
        {
            var clientId = Guid.NewGuid();
            _server = new TethysServer(6969, listenerAdapter: new UdpListenerAdapter(6969));
            var cTask = Task.CompletedTask.ContinueWith(t => { _client = new TethysClient(new UdpClientSocketAdapter("localhost", 6969), clientId); });
            var tcs = new TaskCompletionSource<bool>();
            _server.OnClientConnected += (sender, connectedClient) =>
            {
                Assert.IsTrue(connectedClient.ConnectionId == clientId);
                tcs.SetResult(true);
            };
            var tcsTask = tcs.Task;
            Assert.IsTrue(Task.WhenAny(tcsTask, Task.Delay(TimeSpan.FromSeconds(10))).GetAwaiter().GetResult() == tcsTask, "connection timed out");
            cTask.GetAwaiter().GetResult();
        }
    }
}