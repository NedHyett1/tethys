# Tethys

Tethys is a system for creating a server/client model in .NET in as few lines of end user code as possible. How few, you may ask? Take a look at this example:

### Packet Class
```
[Serializable]
public class NumberWangPacket : TethysPacket 
{

    public int Number { get; set; }
    public bool IsNumberWang { get; set; }

    public override void ProcessPacketServer(ITethysServer server, TethysConnectedClient client)
    {
        var r = new Random();
        Reply(new TestPacket
        {
            IsNumberWang = r.Next() == Number
        });
    }

    public override void ProcessPacketClient(TethysClient client)
    {
        throw new NotImplementedException();
    }

}
```

### Server Program
```

public static void Main(string[] args)
{
    var numberWangServer = new TethysServer(25644);
    Thread.Sleep(Timeout.Infinite);
}

```

### Client Class
```

public static void Main(string[] args)
{
    var r = new Random();
    var numberWangClient = new TethysClient("localhost", 25644);
    var queryNumber = r.Next();
    var responseTask = numberWangClient.WriteAndAwaitResponseAsync<NumberWangPacket>(new NumberWangPacket { Number = queryNumber });
    var response = responseTask.GetAwaiter().GetResult();
    Console.WriteLine($"{queryNumber} is {response.IsNumberWang ? "" : "NOT "}NumberWang!");
}

```

It is that simple!

## How does it work?

Tethys works upon the principle of serialising and deserialising classes and sending them over the network. Packet classes both transfer the data and implement the packet handling (except in cases where the packet is operating as a direct response packet, like shown above). In cases where you want to transfer data between seperate programming languages you can override the default encoding handler.


## Current Features

- Client/Server Model
- Service Discovery
- Client management
- Hot-swappable encoding providers
- Managed raw streams (not fully managed yet)
- Remote Handles API
- SSL

## Planned Features

- Full automatic federation between P2P hosts
  - Including distributed transactional data storage
- Proxy hosts (for proxying Tethys traffic)
- Internet proxy hosts (for proxying generic internet traffic)
- Managed UDP Client/Server
- Channel handles (create a channel handle class that is parented to a connection, a locked-down remote control for the connection that only allows packets on the same channel)
- Abstract the transport layer from the Tethys protocol and support more transport layers (such as bluetooth and more)
- DOS protection
- Rate limiting and backoff
- Ban database
- Proper handling of malformed data
  - Skipping packets with bad magic
  - Packet terminator magic (validate packet length and allow skipping packets)
- Load balancing
- Load measurement
- Adapter selection
- Port Scan for service discovery
- Packet queues
- Tethys Exceptions
- Distinct packet header controls
- Delayed connection
- Hot-swappable compression
- Custom serialisation protocol
- Libraries for multiple languages (planned: Java, C++, Python, PHP, JavaScript, suggest more in issues)
- Chunked packet transfer
- Packet ordering
- Support for automatic connection resolution (if connection fails, try to fix automatically)
- Network analysis
- Tunneling
- "Conversations" to track what packets belong to what request stream (i.e. packet a is sent which causes packet b to be sent which causes packet c, all having the same "conversation" ID)
- Observable objects (objects which react to changes and then replicate their state over a network)
- Remote Handle API to support packet proxying from server to client so client can request handle on another client
- Support for custom authentication protocols
- Remote event invoke
- Service advertising through Tethys